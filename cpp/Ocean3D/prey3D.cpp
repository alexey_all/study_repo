#include"prey3D.h"
#include<stdlib.h>

//public:
Prey3D::Prey3D(const int& x,const int& y,const int& z)
    :Cell3D(x,y,z)
{
    life=Ocean->preysLife+rand()%3-1;
    reproduce=Ocean->preysReproduce+rand()%3-1;
    free=false;
    type=Ocean->preyType;
}
void Prey3D::lifeStep()
{
    int x,y,z;
    if(deadFromOld()) return;
    if(selectFree(x,y,z))
        move(x,y,z);
}
int Prey3D::getLife() const
{
    return life;
}

//protected:
bool Prey3D::deadFromOld()
{
    if(life--<=0)
    {
        Cell3D* tmp=new Cell3D(coord_x,coord_y,coord_z); 
        Ocean->mapOcean[coord_z][coord_y][coord_x]=tmp;
        delete this;
        return true;
    }
    return false;
}
bool Prey3D::Reproduce()
{
    if(reproduce--<=1)
    {
        Ocean->mapOcean[coord_z][coord_y][coord_x]=new Prey3D(coord_x,coord_y,coord_z);
        Ocean->mapOcean[coord_z][coord_y][coord_x]->setUsed(true);
        reproduce=Ocean->preysReproduce;
        return true;
    }
    Ocean->mapOcean[coord_z][coord_y][coord_x]=new Cell3D(coord_x,coord_y,coord_z);
    return false;
}
bool Prey3D::selectFree(int& x,int& y,int& z)
{
    int i=1,j=1,k=1;
    int randx=rand()%3;  
    int randy=rand()%3;
    int randz=rand()%3;

    while(k<4)
    {
        z=coord_z+(randz+k)%3-1;
        if(z>=0 && z<Ocean->img_z)
        {
            while(j<4)
            {
                y=coord_y+(randy+j)%3-1;
                if(y>=0 && y<Ocean->img_y)
                {
                    while(i<4)
                    {
                        x=coord_x+(randx+i)%3-1;
                        if(x>=0 && x<Ocean->img_x)
                        {
                            if(Ocean->mapOcean[z][y][x]->getFree())
                                return true;
                        }
                        i++;
                    }
                }
                j++;
            }
        }
        k++;
    }
    return false;
}
bool Prey3D::move(int& x,int& y,int& z)
{
    delete Ocean->mapOcean[z][y][x];
    Ocean->mapOcean[z][y][x]=this;
    Ocean->mapOcean[z][y][x]->setUsed(true);
    Reproduce();
    Ocean->mapOcean[z][y][x]->setCoords(x,y,z);
    return true;
}