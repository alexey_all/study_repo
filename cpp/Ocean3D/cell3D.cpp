#include"cell3D.h"
#include"ocean3D.h"

Ocean3D* Cell3D::Ocean=0;

Cell3D::Cell3D(const int& x,const int& y,const int& z)
    :type(Ocean->freeType),free(true),used(false)
{
    setCoords(x,y,z);
}

void Cell3D::setCoords(const int& x,const int& y,const int& z)
{
    coord_x=x;
    coord_y=y;
    coord_z=z;
}


