#ifndef _CELL3D_H_

#define _CELL3D_H_
#include"ocean3D.h"

class Cell3D
{
public:
    friend class Ocean3D;

    Cell3D(const int&,const int&,const int&);

    virtual void lifeStep()     {}

    void setUsed(const bool& usd)       {this->used=usd;}
    void setCoords(const int&,const int&,const int&);

    char getType() const        {return type;}
    bool getFree() const        {return free;}
    bool getUsed() const        {return used;}

    int getCoordX() const       {return coord_x;}
    int getCoordY() const       {return coord_y;} 
    int getCoordZ() const       {return coord_z;}

    
protected:
    static Ocean3D* Ocean;      // ����� ������ ������

    bool used;                  // �� ���� �������� ������������ ������ 
    bool free;                  // ��������� ������
    char type;                  // ��� ������
   
    int coord_x;  
    int coord_y;
    int coord_z;    
};
#endif