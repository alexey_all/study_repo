#include<iostream>
#include<stdlib.h>
#include<time.h>
#include"ocean3D.h"

#include"cell3D.h"
#include"stone3D.h"
#include"prey3D.h"
#include"predator3D.h"

using namespace std;

// public:
Ocean3D::Ocean3D(const int& x,const int& y,const int& z)
    :img_x(x),img_y(y),img_z(z)
{
    initOcean();
    predators=preys=stones=0;
}
Ocean3D::Ocean3D(const int& x,const int& y,const int& z,const int& stone,const int& prey,const int& predator)
    :img_x(x),img_y(y),img_z(z),predators(predator),preys(prey),stones(stone)
{
    initOcean();
}
Ocean3D::~Ocean3D()
{
    this->destructMap();
}

void Ocean3D::draw(void) const
{
    for(int z=0;z<this->img_z;z++)
    {
        for(int y=0;y<this->img_y;y++)
        {
            for(int x=0;x<this->img_x;x++)
            {
                switch(mapOcean[z][y][x]->getType()) 
                {
                case freeType:      if(drawFree!=0)     drawFree(mapOcean[z][y][x]);                                break;
                case stoneType:     if(drawStone!=0)    drawStone(static_cast<Stone3D*>(mapOcean[z][y][x]));        break;
                case preyType:      if(drawPrey!=0)     drawPrey(static_cast<Prey3D*>(mapOcean[z][y][x]));          break;
                case predatorType:  if(drawPredator!=0) drawPredator(static_cast<Predator3D*>(mapOcean[z][y][x]));  break;
                }
            }
        }
    }
}
void Ocean3D::calc(void)
{
    countFree=countStones=countPreys=countPredators=0;

    for(int z=0;z<this->img_z;z++)
    {
        for(int y=0;y<this->img_y;y++)
        {
            for(int x=0;x<this->img_x;x++)
            {
                switch(mapOcean[z][y][x]->getType()) 
                {
                case freeType:      countFree++;         break;
                case stoneType:     countStones++;       break;
                case preyType:      countPreys++;        break;
                case predatorType:  countPredators++;    break;
                }
            }
        }
    }
    std::cout<<"Free: "<<countFree<<" Stones: "<<countStones<<" Preys: "<<countPreys<<" Pred: "<<countPredators<<endl;
}
void Ocean3D::randAllocObj(void)
{
    this->destructMap();
    this->createMap();

    srand(time(0));
    int i,x,y,z;
    i=x=y=0;

    while(i<stones)                             // ����������� �����
    {
        x=rand()%img_x;
        y=rand()%img_y; 
        z=rand()%img_z;
        if(mapOcean[z][y][x]->getFree())
        {   
            delete mapOcean[z][y][x];
            mapOcean[z][y][x]=new Stone3D(x,y,z);
            i++;
        }
    }
    i=0;
    while(i<preys)                              // ����������� �����
        {
        x=rand()%img_x;
        y=rand()%img_y; 
        z=rand()%img_z;
        if(mapOcean[z][y][x]->getFree())
        {   
            delete mapOcean[z][y][x];
            mapOcean[z][y][x]=new Prey3D(x,y,z);
            i++;
        }
    }
    i=0;
    while(i<predators)                             // ����������� ��������
    {
        x=rand()%img_x;
        y=rand()%img_y; 
        z=rand()%img_z;
        if(mapOcean[z][y][x]->getFree())
        {   
            delete mapOcean[z][y][x];
            mapOcean[z][y][x]=new Predator3D(x,y,z);
            i++;
        }
    }
}
void Ocean3D::play(void)
{
    for(int z=0;z<this->img_z;z++)
    {
        for(int y=0;y<this->img_y;y++)
        {
            for(int x=0;x<this->img_x;x++)
            {
                if(!(mapOcean[z][y][x]->getUsed()))
                    mapOcean[z][y][x]->lifeStep();
            }
        }
    }
    setUnused();
}

// private:
void Ocean3D::initOcean()
{
    Cell3D::Ocean=this;
    this->createMap();
    predatorsLife=preysLife=10;
    predatorsHLife=5;
    predatorsReproduce=preysReproduce=3;
    drawFree=0;
    drawStone=0;
    drawPrey=0;
    drawPredator=0;
}
void Ocean3D::createMap()
{
    mapOcean = new Cell3D***[this->img_z];

    for(int z=0;z<this->img_z;z++)
    {
        mapOcean[z]=new Cell3D**[this->img_y];
        for(int y=0;y<this->img_y;y++)
        {
            mapOcean[z][y]=new Cell3D*[this->img_x];
            for(int x=0;x<this->img_x;x++)
            {
                mapOcean[z][y][x]=new Cell3D(x,y,z);
            }
        }   
    }
}
void Ocean3D::destructMap()
{
    for(int z=0;z<this->img_z;z++)
    {
        for(int y=0;y<this->img_y;y++)
        {
            for(int x=0;x<this->img_x;x++)
                delete mapOcean[z][y][x];
            delete [] mapOcean[z][y];
        } 
        delete [] mapOcean[z];
    }
    delete [] mapOcean;
}
void Ocean3D::setUnused(void)
{
    for(int z=0;z<this->img_z;z++)
        for(int y=0;y<this->img_y;y++)
            for(int x=0;x<this->img_x;x++)
                mapOcean[z][y][x]->setUsed(false);
}

