#include"predator3D.h"
#include<stdlib.h>

//public:
Predator3D::Predator3D(const int& x,const int& y,const int& z)
    :Prey3D(x,y,z)
{
    life=Ocean->predatorsLife+rand()%3-1;
    hungryLife=Ocean->predatorsHLife+rand()%3-1;
    reproduce=Ocean->predatorsReproduce+rand()%3-1;
    free=false;
    type=Ocean->predatorType;
}
void Predator3D::lifeStep()
{
    int x,y,z;
    if(deadFromOld()) return;
    if(deadFromHungry()) return;
    if(selectPreyAround(x,y,z))
        move(x,y,z);
    else if(selectFree(x,y,z))
        move(x,y,z);
}
void Predator3D::eat(const int& actToDie)
{
    
}

//protected:
bool Predator3D::deadFromHungry()
{
    if(hungryLife--<=0)
    {
        Cell3D* tmp=new Cell3D(coord_x,coord_y,coord_z); 
        Ocean->mapOcean[coord_z][coord_y][coord_x]=tmp;
        delete this;
        return true;
    }
    return false;
}
bool Predator3D::Reproduce()
{
    if(reproduce--<=1)
    {
        Ocean->mapOcean[coord_z][coord_y][coord_x]=new Predator3D(coord_x,coord_y,coord_z);
        Ocean->mapOcean[coord_z][coord_y][coord_x]->setUsed(true);
        reproduce=Ocean->predatorsReproduce;
        return true;
    }
    Ocean->mapOcean[coord_z][coord_y][coord_x]=new Cell3D(coord_x,coord_y,coord_z);
    return false;
}
bool Predator3D::selectPreyAround(int& x,int& y,int& z)
{
    for(z=coord_z-1;z<=coord_z+1;z++)
    {
        for(y=coord_y-1;y<=coord_y+1;y++)
        {
            for(x=coord_x-1;x<=coord_x+1;x++)
            {
                if(x>=0 && x<Ocean->img_x && y>=0 && y<Ocean->img_y && z>=0 && z<Ocean->img_z)
                {
                    if(Ocean->mapOcean[z][y][x]->getType()==Ocean->preyType)
                    {
                        hungryLife=Ocean->predatorsHLife+rand()%3-1;
                        return true;  
                    }
                }
            }
        }
    }
    return false;
}
