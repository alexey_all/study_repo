#include <GL/glut.h>
#include <math.h>
#include <stdlib.h>
#include "ocean3D.h"
#include "stone3D.h"
#include "prey3D.h"
#include "predator3D.h"

const GLdouble canv_w=1024;                     // Canvas width
const GLdouble canv_h=768;                      // Canvas height

const int grid_w=20;                            // Grid width
const int grid_h=15;                            // Grid height
const int grid_d=5;                             // Grid depth

const int stones=(grid_w*grid_h*grid_d)*0.01;   // Number of stones - 10% of the screen 
const int predators=(grid_w*grid_h*grid_d)*0.01;// Number of predators - 10% of the screen  
const int preys=(grid_w*grid_h*grid_d)*0.01;    // Number of preys - 10% of the screen  

const int preysLife=15;                         // +-1
const int preysReproduce=7;                     // +-1
const int predatorsLife=30;                     // +-1
const int predatorsHungryLife=10;               // +-1
const int predatorsReproduce=12;                // +-1

const float element_sz_x=canv_w/grid_w;         // Size X of one element
const float element_sz_y=canv_h/grid_h;         // Size Y of one element
const float element_sz_z=element_sz_y;          // Size Z of one element
const int maxX=canv_w;
const int minX=0;
const int maxY=canv_h;
const int minY=0;
const int maxZ=canv_h;
const int minZ=-canv_h;
const float centerX=(maxX+minX)/2;
const float centerY=(maxY+minY)/2;
const float centerZ=0;

const float PI=3.1415926;

GLfloat spinX=0.0;
GLfloat spinY=0.0;
GLfloat spinZ=0.0;

Ocean3D* oceanium;

void init(void)
{
    glClearColor(0.2,0.2,0.2,0.0);                      //������� ����� � ����, ������������ ����������� r,g,b,a
    glShadeModel(GL_SMOOTH);                            // ��� ������������

    glEnable(GL_DEPTH_TEST);                            // �������� �������� �������
    glEnable(GL_CULL_FACE);                             // �� �������� �������� �������


       
//    glEnable(GL_COLOR_MATERIAL);
//
//    glEnable(GL_LIGHTING);                              //��� �� �������� ������ ��������� 
//    glEnable(GL_LIGHT0);
//    glLightModelf(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);    //������ ���, ����� ���������� ��� ������� �������� 
//    glEnable(GL_NORMALIZE);                             //����� ������� ���������� �������� �� ��������� ���������� 
//    
//    
// float light_ambient[] = {0.0,0.0,0.0,1.0};
//float light_diffuse[] = {1.0,1.0,1.0,1.0};
//float light_specular[] = {1.0,1.0,1.0,1.0};
//float light_position[] = {400.0,300.0,500.0,1.0};
//
//glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
//glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
//glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
//glLightfv(GL_LIGHT0, GL_POSITION, light_position);
//    glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
}

void drawGrid2D(void)
{
    glBegin(GL_LINES);
        glColor3f(0.5,0.5,0.5);
        for(int i=0;i<=grid_w;i+=1)
        {
            glVertex3f(i*element_sz_x,minY,centerZ);
            glVertex3f(i*element_sz_x,grid_h*element_sz_y,centerZ);
        }
        for(int j=0;j<=grid_h;j+=1)
        {
            glVertex3f(minX,j*element_sz_y,centerZ);
            glVertex3f(grid_w*element_sz_x,j*element_sz_y,centerZ);
        }
    glEnd();
}
void drawGrid3D(void)
{
    glBegin(GL_LINES);
        glColor3f(0.5,0.5,0.5);
        for(int i=0;i<=grid_w;i+=1)
        {
            glVertex3f(i*element_sz_x,minY,centerZ);
            glVertex3f(i*element_sz_x,grid_h*element_sz_y,centerZ);
        }
        for(int j=0;j<=grid_h;j+=1)
        {
            glVertex3f(minX,j*element_sz_y,centerZ);
            glVertex3f(grid_w*element_sz_x,j*element_sz_y,centerZ);
        }
    glEnd();
    glBegin(GL_LINE_STRIP);
        glVertex3f(minX,minY,centerZ);
        glVertex3f(minX,maxY,centerZ);
        glVertex3f(minX,maxY,grid_d*element_sz_z);
        glVertex3f(minX,minY,grid_d*element_sz_z);
        glVertex3f(minX,minY,centerZ);
    glEnd();
    glBegin(GL_LINE_STRIP);
        glVertex3f(maxX,minY,centerZ);
        glVertex3f(maxX,maxY,centerZ);
        glVertex3f(maxX,maxY,grid_d*element_sz_z);
        glVertex3f(maxX,minY,grid_d*element_sz_z);
        glVertex3f(maxX,minY,centerZ);
    glEnd();
}
void drawCoord(void)
{
    glBegin(GL_LINES);  // coordinate system
        glColor3f(0.0f,0.0f,1.0f); //z -axle
        glVertex3f(minX+10,minY+10,centerZ);
        glVertex3f(minX+10,minY+10,grid_d*element_sz_z);
        glColor3f(0.0f,1.0f,0.0f); //y -axle
        glVertex3f(minX+10,minY+10,centerZ);
        glVertex3f(minX+10,maxY-10,centerZ);
        glColor3f(1.0f,0.0f,0.0f); //x -axle
        glVertex3f(minX+10,minY+10,centerZ);
        glVertex3f(maxX-10,minY+10,centerZ);
    glEnd();
}

void drawFish(const Prey3D* fish)
{

    GLfloat color=0.7f*fish->getLife()/preysLife;
    if(color>1) color=1;

    glPushMatrix();                                                         // ��������� ������� ����������
        glTranslatef(fish->getCoordX()*element_sz_x,fish->getCoordY()*element_sz_y,
            fish->getCoordZ()*element_sz_z);                                // ���������� �� ������� ������

        glBegin(GL_LINE_LOOP);   
            glColor3f(0.0f,0.3f+color,1.0f);
            glVertex3f(element_sz_x*0.1, element_sz_y*0.5, 0);
            glVertex3f(element_sz_x*0.2, element_sz_y*0.5, 0);
            glVertex3f(element_sz_x*0.3, element_sz_y*0.6, 0);
            glVertex3f(element_sz_x*0.4, element_sz_y*0.6, 0);
            glVertex3f(element_sz_x*0.5, element_sz_y*0.7, 0);
            glVertex3f(element_sz_x*0.7, element_sz_y*0.5, 0);
            glVertex3f(element_sz_x*0.8, element_sz_y*0.5, 0);
            glVertex3f(element_sz_x*0.9, element_sz_y*0.6, 0);
            glVertex3f(element_sz_x*0.9, element_sz_y*0.5, 0);
            glColor3f(0.8f,0.8f,0.8f);
            glVertex3f(element_sz_x*0.8, element_sz_y*0.4, 0);
            glVertex3f(element_sz_x*0.9, element_sz_y*0.3, 0);
            glVertex3f(element_sz_x*0.9, element_sz_y*0.2, 0);
            glVertex3f(element_sz_x*0.8, element_sz_y*0.3, 0);
            glVertex3f(element_sz_x*0.7, element_sz_y*0.3, 0);
            glVertex3f(element_sz_x*0.6, element_sz_y*0.2, 0);
            glVertex3f(element_sz_x*0.3, element_sz_y*0.2, 0);
            glVertex3f(element_sz_x*0.2, element_sz_y*0.3, 0);
            glVertex3f(element_sz_x*0.1, element_sz_y*0.3, 0);
            glVertex3f(element_sz_x*0.2, element_sz_y*0.4, 0);
        glEnd();

    glPopMatrix(); 
}
void drawShark(const Predator3D* fish)
{
    int x,y,z;
    GLfloat color;
    x=fish->getCoordX();
    y=fish->getCoordY();
    z=fish->getCoordZ();
    color=1.0f*fish->getLife()/predatorsLife;
    if(color>1) color=1;

    glBegin(GL_LINE_LOOP);   
        glColor3f(1.0f,0.0f+color,0.0f);
        glVertex3f((x+0.1)*element_sz_x, (y+0.5)*element_sz_y, (z+0.5)*element_sz_z);
        glVertex3f((x+0.2)*element_sz_x, (y+0.5)*element_sz_y, (z+0.5)*element_sz_z);
        glVertex3f((x+0.3)*element_sz_x, (y+0.6)*element_sz_y, (z+0.5)*element_sz_z);
        glVertex3f((x+0.4)*element_sz_x, (y+0.6)*element_sz_y, (z+0.5)*element_sz_z);
        glVertex3f((x+0.5)*element_sz_x, (y+0.7)*element_sz_y, (z+0.5)*element_sz_z);
        glVertex3f((x+0.7)*element_sz_x, (y+0.5)*element_sz_y, (z+0.5)*element_sz_z);
        glVertex3f((x+0.8)*element_sz_x, (y+0.5)*element_sz_y, (z+0.5)*element_sz_z);
        glVertex3f((x+0.9)*element_sz_x, (y+0.6)*element_sz_y, (z+0.5)*element_sz_z);
        glVertex3f((x+0.9)*element_sz_x, (y+0.5)*element_sz_y, (z+0.5)*element_sz_z);
        glVertex3f((x+0.8)*element_sz_x, (y+0.4)*element_sz_y, (z+0.5)*element_sz_z);
        glVertex3f((x+0.9)*element_sz_x, (y+0.3)*element_sz_y, (z+0.5)*element_sz_z);
        glVertex3f((x+0.9)*element_sz_x, (y+0.2)*element_sz_y, (z+0.5)*element_sz_z);
        glVertex3f((x+0.8)*element_sz_x, (y+0.3)*element_sz_y, (z+0.5)*element_sz_z);
        glVertex3f((x+0.7)*element_sz_x, (y+0.3)*element_sz_y, (z+0.5)*element_sz_z);
        glVertex3f((x+0.6)*element_sz_x, (y+0.2)*element_sz_y, (z+0.5)*element_sz_z);
        glVertex3f((x+0.3)*element_sz_x, (y+0.2)*element_sz_y, (z+0.5)*element_sz_z);
        glVertex3f((x+0.2)*element_sz_x, (y+0.3)*element_sz_y, (z+0.5)*element_sz_z);
        glVertex3f((x+0.1)*element_sz_x, (y+0.3)*element_sz_y, (z+0.5)*element_sz_z);
        glVertex3f((x+0.2)*element_sz_x, (y+0.4)*element_sz_y, (z+0.5)*element_sz_z);
    glEnd();

    
    //glBegin(GL_POLYGON);   
    //    glColor3f(1.0f,0.0f+color,0.0f);
    //    glEdgeFlag(GL_TRUE);
    //    glVertex3f((x+0.1)*element_sz_x, (y+0.5)*element_sz_y, (z+0.5)*element_sz_z);
    //    glEdgeFlag(GL_FALSE);
    //    glVertex3f((x+0.2)*element_sz_x, (y+0.5)*element_sz_y, (z+0.5)*element_sz_z);
    //    glVertex3f((x+0.3)*element_sz_x, (y+0.6)*element_sz_y, (z+0.5)*element_sz_z);
    //    glVertex3f((x+0.4)*element_sz_x, (y+0.3)*element_sz_y, (z+0.5)*element_sz_z);

    //    /*glVertex3f((x+0.)*element_sz_x, (y+0.)*element_sz_y, (z+0.)*element_sz_z);*/
    //glEnd();
}
void drawStone(const Stone3D* fish)
{
    int x,z;

    glPushMatrix();                                                         // ��������� ������� ����������
        glTranslated(fish->getCoordX()*element_sz_x,fish->getCoordY()*element_sz_y,
            fish->getCoordZ()*element_sz_z);                                // ���������� �� ������� ������
        
        // ������
        glBegin(GL_TRIANGLE_FAN);
            glColor3f(0.6f,0.8f,0.6f);
            glVertex3f(0.2*element_sz_x, 0.6*element_sz_y, 0.3*element_sz_z);
            for(float angle=0.0f; angle<=360; angle+=20)  // ������ ����������
		    {
                glColor3f(0.0f,0.0f,0.0f);
    		    x = element_sz_x*sin(angle)*0.1+element_sz_x*0.4;
    		    z = element_sz_z*cos(angle)*0.1+element_sz_z*0.4;
		        glVertex3f(x, 0, z);
		    }
        glEnd();

        glBegin(GL_TRIANGLE_FAN);
            glColor3f(0.5f,0.4f,0.4f);
            glVertex3f(0.6*element_sz_x, 0.8*element_sz_y, 0.7*element_sz_z);
            for(float angle=0.0f; angle<=360; angle+=20)  // ������ ����������
		    {
                glColor3f(0.0f,0.0f,0.0f);
    		    x = element_sz_x*sin(angle)*0.1+element_sz_x*0.4;
    		    z = element_sz_z*cos(angle)*0.1+element_sz_z*0.4;
		        glVertex3f(x, 0, z);
		    }
        glEnd();

        glBegin(GL_TRIANGLE_FAN);
            glColor3f(0.7f,0.6f,0.6f);
            glVertex3f(0.8*element_sz_x, 0.5*element_sz_y, 0.4*element_sz_z);
            for(float angle=0.0f; angle<=360; angle+=20)  // ������ ����������
		    {
                glColor3f(0.0f,0.0f,0.0f);
    		    x = element_sz_x*sin(angle)*0.1+element_sz_x*0.4;
    		    z = element_sz_z*cos(angle)*0.1+element_sz_z*0.4;
		        glVertex3f(x, 0, z);
		    }
        glEnd();

        glBegin(GL_TRIANGLE_FAN);
            glColor3f(0.5f,0.5f,0.5f);
            glVertex3f(0.8*element_sz_x, 0.3*element_sz_y, 0.7*element_sz_z);
            for(float angle=0.0f; angle<=360; angle+=20)  // ������ ����������
		    {
                glColor3f(0.3f,0.3f,0.3f);
    		    x = element_sz_x*sin(angle)*0.1+element_sz_x*0.7;
    		    z = element_sz_z*cos(angle)*0.1+element_sz_z*0.5;
		        glVertex3f(x, 0, z);
		    }
        glEnd();

        // ������
        glBegin(GL_TRIANGLE_FAN);
            glColor3f(0.3f,0.3f,0.3f);
            glVertex3f(0.4*element_sz_x, 0, 0.4*element_sz_z);
            for(float angle=360; angle>=0.0; angle-=20)  // ������ ����������
		    {
                glColor3f(0.0f,0.0f,0.0f);
    		    x = element_sz_x*sin(angle)*0.1+element_sz_x*0.4;
    		    z = element_sz_z*cos(angle)*0.1+element_sz_z*0.4;
		        glVertex3f(x, 0, z);
		    }
        glEnd();

        glBegin(GL_TRIANGLE_FAN);
            glColor3f(0.3f,0.3f,0.3f);
            glVertex3f(0.7*element_sz_x, 0, 0.5*element_sz_z);
            for(float angle=360; angle>=0.0; angle-=20)  // ������ ����������
		    {
                glColor3f(0.0f,0.0f,0.0f);
    		    x = element_sz_x*sin(angle)*0.1+element_sz_x*0.7;
    		    z = element_sz_z*cos(angle)*0.1+element_sz_z*0.5;
		        glVertex3f(x, 0, z);
		    }
        glEnd();

    glPopMatrix(); 
}
void drawStoneDynamic(const Stone3D* fish)
{
    bool arrCreated=false;
    double x_up,y_up,z_up,x_down,z_down,x_tmp,z_tmp;

    glPushMatrix();                                                         // ��������� ������� ����������
        glTranslated(fish->getCoordX()*element_sz_x,fish->getCoordY()*element_sz_y,
            fish->getCoordZ()*element_sz_z);                                // ���������� �� ������� ������
        
        for(int i=0; i<10; i++)
        {   
            x_up=rand()%8*0.1+0.1;
            y_up=rand()%8*0.1+0.1;
            z_up=rand()%8*0.1+0.1;
            x_down=rand()%8*0.1+0.1;
            z_down=rand()%8*0.1+0.1;

            // ������
            glBegin(GL_TRIANGLE_FAN);
                glColor3f(0.6f,0.6f,0.6f);
                glVertex3f(x_up*element_sz_x, y_up*element_sz_y, z_up*element_sz_z);
                for(float angle=0.0f; angle<=360; angle+=20)  // ������ ����������
		        {
                    glColor3f(0.0f,0.0f,0.0f);
    		        x_tmp = element_sz_x*sin(angle)*0.1+element_sz_x*x_down;
    		        z_tmp = element_sz_z*cos(angle)*0.1+element_sz_z*z_down;
		            glVertex3f(x_tmp, 0, z_tmp);
		        }
            glEnd();
        
            // ������
            glBegin(GL_TRIANGLE_FAN);
                glColor3f(0.3f,0.3f,0.3f);
                glVertex3f(x_down*element_sz_x, 0, z_down*element_sz_z);
                for(float angle=360; angle>=0.0; angle-=20)  // ������ ����������
		        {
                    glColor3f(0.0f,0.0f,0.0f);
    		        x_tmp = element_sz_x*sin(angle)*0.1+element_sz_x*x_down;
    		        z_tmp = element_sz_z*cos(angle)*0.1+element_sz_z*z_down;
		            glVertex3f(x_tmp, 0, z_tmp);
		        }
            glEnd();
        }

    glPopMatrix(); 
}

void display(void) 
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glPushMatrix();       // ��������� ������� ����������
        // glScalef(0.5,0.9,1.0);  // c������ �������
    
    
        glRotatef(spinX,1.0f,1.0f,0.0f); // �������� �� ���� �� ��� �������
        drawGrid3D();
        drawCoord();
        oceanium->draw();

    //glTranslated(centerX,centerY,0);  // ���������� �� ��� � �� 100

    glPopMatrix();  // ������������ � ������ ������� ���������
    glutSwapBuffers();
}
void reshape(int w, int h)
{
    glViewport(0,0,(GLsizei) w, (GLsizei) h);                   // ��������������� ��� ������ ����
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();                                           // ������� � ������� ������� 
    //glOrtho(minX-100,maxX+100,minY-100,maxY+100,minZ-100,maxZ+100);                    // ��������� ���������� ���������
    gluPerspective(10,(GLfloat)w/h,minZ-100,maxZ+100);
    glTranslated(-centerX,-centerY,-5000);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();                                           // ������� � ������� ������� � ���� �������
    
}
void spinDisplay(void)
{
    spinX++; spinY++;
    if(spinX>360.0) spinX=spinX-360;
    if(spinY>360.0) spinY=spinY-360;
    glutPostRedisplay();
}

void mousePressed(int button,int state,int x,int y)
{
    glutSetWindowTitle("Mouse pressed");
    switch(button)
    {
    case GLUT_LEFT_BUTTON:  if (state==GLUT_DOWN) glutIdleFunc(spinDisplay); break;
    case GLUT_RIGHT_BUTTON: if (state==GLUT_DOWN) glutIdleFunc(0); break;
    case GLUT_MIDDLE_BUTTON: if (state==GLUT_DOWN) glutIdleFunc(0); /*spinX=spinY=0;*/ oceanium->randAllocObj(); display(); break;
    }
    //state GLUT_UP,GLUT_DOWN
}
void mousePressedMove(int x,int y)
{
    glutSetWindowTitle("Mouse pressed move");
}
void mouseMove(int x,int y)
{
    glutSetWindowTitle("Mouse move");
    glutPostRedisplay();
}
void keyfun(unsigned char ch,int x,int y) 
{
    switch(ch)
    {
    case 27 :
    case 'q' : exit(0); break;
    case 'w' : spinX+=1.0; glutPostRedisplay(); break;
    case 's' : spinX-=1.0; glutPostRedisplay(); break;
    case 'a' : spinY+=1.0; glutPostRedisplay(); break;
    case 'd' : spinY-=1.0; glutPostRedisplay(); break;
    case 'c' : spinX=spinY=0; glutPostRedisplay(); break;
    default: glutPostRedisplay(); oceanium->play(); oceanium->calc();
    }  
}
void skeyfun(int key,int x,int y) 
{
    switch(key)
    {
    case GLUT_KEY_UP : spinX+=1.0; glutPostRedisplay(); break;
    case GLUT_KEY_DOWN : spinX-=1.0; glutPostRedisplay(); break;
    case GLUT_KEY_LEFT : spinY+=1.0; glutPostRedisplay(); break;
    case GLUT_KEY_RIGHT : spinY-=1.0; glutPostRedisplay(); break;
    default: glutPostRedisplay(); oceanium->play(); oceanium->calc();
    }  
}
int main(int argc, char **argv)
{
    oceanium = new Ocean3D(grid_w,grid_h,grid_d,stones,preys,predators);
    oceanium->setPreysLife(preysLife);
    oceanium->setPreysReproduce(preysReproduce);
    oceanium->setPredatorsLife(predatorsLife);
    oceanium->setPredatorsHLife(predatorsHungryLife);
    oceanium->setPredatorsReproduce(predatorsReproduce);
    oceanium->setFuncDrawPrey(drawFish);
    oceanium->setFuncDrawPredator(drawShark);
    oceanium->setFuncDrawStone(drawStone);
    oceanium->randAllocObj();


    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB);
    glutInitWindowSize(canv_w,canv_h);
    glutInitWindowPosition(100,100);
    glutCreateWindow("������������� ����� ������");
    init();
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutMouseFunc(mousePressed);
    glutMotionFunc(mousePressedMove);
    glutPassiveMotionFunc(mouseMove);
    glutKeyboardFunc(keyfun);
    glutSpecialFunc(skeyfun);
    glutMainLoop();
    return 0;
}