#ifndef _PREY3D_H_
#define _PREY3D_H_
#include "cell3D.h"

class Prey3D: public Cell3D
{
public:
    Prey3D(const int&,const int&,const int&);
    virtual void lifeStep(void);
    virtual int getLife(void) const;

protected:
    int life;
    int reproduce;

    virtual bool Reproduce(void);           // ���������� ������� ��� ������ ������
    bool deadFromOld(void);                 // ������ �� ��������
    bool selectFree(int&,int&,int&);        // ���������� ���������� ������������ ��������� �������� ������
    bool move(int&,int&,int&);              // ������� �� ����� ������� � ������������ �����������
};

#endif