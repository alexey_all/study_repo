#ifndef _OCEAN3D_H_
#define _OCEAN3D_H_

class Cell3D;

class Ocean3D
{    
public:
    friend class Cell3D;
    friend class Stone3D;
    friend class Prey3D;
    friend class Predator3D;

    Ocean3D(const int&,const int&,const int&);          // x,y,z
    Ocean3D(const int&,const int&,const int&,const int&,const int&,const int&); //x,y,z,stones,preys,predators
    ~Ocean3D();

    void setStones(const int& cnt)              {if(cnt>=0) stones=cnt;}                // ������������� ����� (default=0)

    void setPreys(const int& cnt)               {if(cnt>=0)  preys=cnt;}                // ������������� ������ (default=0)
    void setPreysLife(const int& life_t)        {if(life_t>=0) preysLife=life_t;}       // ������������� ����� ����� ����� (default=10)
    void setPreysReproduce(const int& cnt)      {if(cnt>=0) preysReproduce=cnt;}        // ������������� ����� ����������� ����� (default=3)

    void setPredators(const int& cnt)           {if(cnt>=0) predators=cnt;}             // ������������� �������� (default=0)
    void setPredatorsLife(const int& life_t)    {if(life_t>=0) predatorsLife=life_t;}   // ������������� ����� ����� �������� (default=10) � 
    void setPredatorsHLife(const int& life_t)   {if(life_t>=0) predatorsHLife=life_t;}  // ������ �� ������ (default=5)
    void setPredatorsReproduce(const int& cnt)  {if(cnt>=0) predatorsReproduce=cnt;}    // ������������� ����� ����������� �������� (default=3)

    void setLifeTolerance(const int& cnt)       {if(cnt>=0) lifeTolerance=cnt;}         // ������������� ������ �� ������� �����
    void setReproduceTolerance(const int& cnt)  {if(cnt>=0) reproduceTolerance=cnt;}    // ������������� ������ �� ������� �����������

    void setFuncDrawFree(void (*p)(const Cell3D*))                  {drawFree=p;}   
    void setFuncDrawStone(void (*p)(const Stone3D*))                {drawStone=p;}
    void setFuncDrawPrey(void (*p)(const Prey3D*))                  {drawPrey=p;}
    void setFuncDrawPredator(void (*p)(const Predator3D*))          {drawPredator=p;}

    void draw(void) const;                              // �����������
    void calc(void);                                    // ��������� ���-��
    void randAllocObj(void);                            // ������������ ������������� �������� �� �����
    void play(void);                                    // ��������� ������������� �����



private:                     
    Cell3D**** mapOcean;                                // ��������� �� ���������� �����

    int img_x;                                          // ������� ������
    int img_y;
    int img_z;

    static const char freeType =   ' ';                 // ����������� ���� �������
    static const char stoneType =  '#';
    static const char preyType =   'f';
    static const char predatorType='S';

    int lifeTolerance;                                  // ������ �� ������� �����
    int reproduceTolerance;                             // ������ �� ������� �������� ��������

    int stones;                                         // ���-�� ������

    int predators;
    int predatorsLife;
    int predatorsHLife;
    int predatorsReproduce;

    int preys;
    int preysLife;
    int preysReproduce;

    void (*drawFree)(const Cell3D* cell);               // ����� ������� ��������� ������ ��������� �������
    void (*drawStone)(const Stone3D* cell);
    void (*drawPrey)(const Prey3D* cell);
    void (*drawPredator)(const Predator3D* cell);

    void initOcean(void);                               // ������������� ��������� (����� ������������)
    void createMap(void);                               // ������������� ������� ����������
    void destructMap(void);                             // ������������ ������� ����������
    void setUnused();                                   // ������� ��������� ��� ���������� �����

    int countFree;
    int countStones;
    int countPreys;
    int countPredators;  
};


#endif