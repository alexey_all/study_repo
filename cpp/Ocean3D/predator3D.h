#ifndef _PREDATOR3D_H_
#define _PREDATOR3D_H_
#include "prey3D.h"

class Predator3D: public Prey3D
{
public:
    Predator3D(const int&,const int&,const int&);
    virtual void lifeStep(void);
    void eat(const int&);

protected:
    int hungryLife;

    bool deadFromHungry(void);                 // ������ �� ������
    virtual bool Reproduce(void);
    bool selectPreyAround(int&,int&,int&);
};

#endif