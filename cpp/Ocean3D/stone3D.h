#ifndef _STONE3D_H_
#define _STONE3D_H_
#include"cell3D.h"

class Stone3D: public Cell3D
{
public:
    Stone3D(const int& x,const int& y,const int& z) :Cell3D(x,y,z)
    {
        Cell3D::free=false;
        Cell3D::type=Ocean->stoneType; 
    }
};

#endif