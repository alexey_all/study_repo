#include<iostream>
#include<Windows.h>
#include<time.h>
#include"dt.h"
using namespace std;

Dt::Dt()
{
    time_t now;
    time(&now);
    dt=*(localtime(&now));
}

Dt::Dt(int d,int m ,int y)
{
    set(d, m, y);
}

void Dt::set(int d,int m ,int y)
{
    dt.tm_mday=d;
    dt.tm_mon=m-1;
    dt.tm_year=y-1900;
    dt.tm_hour=dt.tm_isdst=dt.tm_min=dt.tm_sec=dt.tm_wday=dt.tm_yday=0;
}

void Dt::printToday()
{
    strftime(buf,29,"%d-%b-%Y",&dt);
    cout<<buf<<endl;
}

void Dt::printTomorrow()
{
    struct tm tmp;
    time_t tt=mktime(&dt)+24*60*60;
    tmp=*(localtime(&tt));
    strftime(buf,29,"%d-%b-%Y",&tmp);
    cout<<buf<<endl;
}
    
void Dt::printYesterday()
{
    struct tm tmp;
    time_t tt=mktime(&dt)-24*60*60;
    tmp=*(localtime(&tt));
    strftime(buf,29,"%d-%b-%Y",&tmp);
    cout<<buf<<endl;
}
    
void Dt::printDate(int i)
{
    struct tm tmp;
    time_t tt=mktime(&dt)+24*60*60*i;
    tmp=*(localtime(&tt));
    strftime(buf,29,"%d-%b-%Y",&tmp);
    cout<<buf<<endl;
}