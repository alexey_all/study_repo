#ifndef _DT_H_
#define _DT_H_
#include<time.h>

class Dt
{
public:
    Dt();
    Dt(int,int,int);
    void set(int,int,int);
    void printToday();
    void printTomorrow();
    void printYesterday();
    void printDate(int);
private:
    struct tm dt;
    char buf[30];
};

#endif