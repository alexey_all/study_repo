#include"mine.h"
#include<iostream>
#include<Windows.h>
using namespace std;

MINE::MINE()
{
    isBang=0;
    isActivate=0;
    time=0;
    pressesToBang=0;
};

MINE::~MINE()
{
    if(isBang==0)
        cout<<"Mine is not Bang"<<endl;
};

void MINE::activate()
{
    isActivate=1;
};

void MINE::deactivate()
{
    isActivate=0;
};

void MINE::press()
{
    if(isActivate==1 && pressesToBang==0)
    {
        if(time==0)
            bang();
        else
        {
            while(time)
            {
                time--;
                Sleep(1000);
                if(time==0)
                    bang();
            }
        }
    }
    else if(isActivate==1 && pressesToBang>0)
        pressesToBang--;
};

void MINE::setFreePresses(int pr)
{
    pressesToBang=pr;
};

void MINE::settime(int tm)
{
    time=tm;
};

void MINE::bang()
{
    isBang=1;
    cout<<"!!!BANG!!!"<<endl;
};