#ifndef _MINE_H_
#define _MINE_H_

class MINE
{
public:
    MINE();
    ~MINE();
    void activate();
    void deactivate();
    void press();
    void setFreePresses(int);
    void settime(int);
private:
    void bang();
    int isBang;
    int isActivate;
    int time;
    int pressesToBang;
};

#endif