#include"prey.h"
#include<gl\glut.h>
#include<iostream>
using namespace std;

//public:
Prey::Prey(const int& x,const int& y)
    :Cell(x,y,'f',false)
{
}

Prey::Prey(const int& x,const int& y,const int& life_t,const int& repr)
    :Cell(x,y,'f',false,life_t,5,repr)
{
}

Prey::Prey(const Cell ptr, const int& x,const int& y)
    :Cell(x,y,'f',false,ptr.getLifeTime(),ptr.getActToDie(),ptr.getActToRepr())
{
}

Prey::Prey(const Cell* ptr, const int& x,const int& y)
    :Cell(x,y,'f',false,ptr->getLifeTime(),ptr->getActToDie(),ptr->getActToRepr())
{
}

void Prey::draw(const int& sz_x,const int& sz_y) const
{
    glBegin(GL_LINE_LOOP);   
        glColor3f(1.0f,1.0f,actionToDie*0.1);
        glVertex3f((Cell::getCoordX()*sz_x)+(sz_x*0.1), (Cell::getCoordY()*sz_y)+(sz_y*0.5), 0.0f);
        glVertex3f((Cell::getCoordX()*sz_x)+(sz_x*0.2), (Cell::getCoordY()*sz_y)+(sz_y*0.5), 0.0f);
        glVertex3f((Cell::getCoordX()*sz_x)+(sz_x*0.3), (Cell::getCoordY()*sz_y)+(sz_y*0.6), 0.0f);
        glVertex3f((Cell::getCoordX()*sz_x)+(sz_x*0.4), (Cell::getCoordY()*sz_y)+(sz_y*0.6), 0.0f);
        glVertex3f((Cell::getCoordX()*sz_x)+(sz_x*0.5), (Cell::getCoordY()*sz_y)+(sz_y*0.7), 0.0f);
        glVertex3f((Cell::getCoordX()*sz_x)+(sz_x*0.7), (Cell::getCoordY()*sz_y)+(sz_y*0.5), 0.0f);
        glVertex3f((Cell::getCoordX()*sz_x)+(sz_x*0.8), (Cell::getCoordY()*sz_y)+(sz_y*0.5), 0.0f);
        glVertex3f((Cell::getCoordX()*sz_x)+(sz_x*0.9), (Cell::getCoordY()*sz_y)+(sz_y*0.6), 0.0f);
        glVertex3f((Cell::getCoordX()*sz_x)+(sz_x*0.9), (Cell::getCoordY()*sz_y)+(sz_y*0.5), 0.0f);
        glVertex3f((Cell::getCoordX()*sz_x)+(sz_x*0.8), (Cell::getCoordY()*sz_y)+(sz_y*0.4), 0.0f);
        glVertex3f((Cell::getCoordX()*sz_x)+(sz_x*0.9), (Cell::getCoordY()*sz_y)+(sz_y*0.3), 0.0f);
        glVertex3f((Cell::getCoordX()*sz_x)+(sz_x*0.9), (Cell::getCoordY()*sz_y)+(sz_y*0.2), 0.0f);
        glVertex3f((Cell::getCoordX()*sz_x)+(sz_x*0.8), (Cell::getCoordY()*sz_y)+(sz_y*0.3), 0.0f);
        glVertex3f((Cell::getCoordX()*sz_x)+(sz_x*0.7), (Cell::getCoordY()*sz_y)+(sz_y*0.3), 0.0f);
        glVertex3f((Cell::getCoordX()*sz_x)+(sz_x*0.6), (Cell::getCoordY()*sz_y)+(sz_y*0.2), 0.0f);
        glVertex3f((Cell::getCoordX()*sz_x)+(sz_x*0.3), (Cell::getCoordY()*sz_y)+(sz_y*0.2), 0.0f);
        glVertex3f((Cell::getCoordX()*sz_x)+(sz_x*0.2), (Cell::getCoordY()*sz_y)+(sz_y*0.3), 0.0f);
        glVertex3f((Cell::getCoordX()*sz_x)+(sz_x*0.1), (Cell::getCoordY()*sz_y)+(sz_y*0.3), 0.0f);
        glVertex3f((Cell::getCoordX()*sz_x)+(sz_x*0.2), (Cell::getCoordY()*sz_y)+(sz_y*0.4), 0.0f);
    glEnd();
}
