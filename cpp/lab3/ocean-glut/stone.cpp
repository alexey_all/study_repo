#include"stone.h"
#include<iostream>
#include<gl\glut.h>
using namespace std;

Stone::Stone(const int& x,const int& y)
    :Cell(x,y,'#',false)
{
}

void Stone::draw(const int& sz_x,const int& sz_y) const
{
    glBegin(GL_LINE_LOOP);   
        glColor3f(0.8f,0.8f,0.8f);
        glVertex3f((Cell::getCoordX()*sz_x)+(sz_x*0.1), (Cell::getCoordY()*sz_y)+(sz_y*0.1), 0.0f);
        glVertex3f((Cell::getCoordX()*sz_x)+(sz_x*0.9), (Cell::getCoordY()*sz_y)+(sz_y*0.1), 0.0f);
        glVertex3f((Cell::getCoordX()*sz_x)+(sz_x*0.9), (Cell::getCoordY()*sz_y)+(sz_y*0.9), 0.0f);
        glVertex3f((Cell::getCoordX()*sz_x)+(sz_x*0.1), (Cell::getCoordY()*sz_y)+(sz_y*0.9), 0.0f);
    glEnd();
}