#include <GL/glut.h>
#include<math.h>
#include<stdlib.h>
#include"myocean.h"

const GLdouble canv_w=800;                      // Canvas width
const GLdouble canv_h=600;                      // Canvas height
const int grid_w=20;                            // Grid count width
const int grid_h=15;                            // Grid count height

const int stones=(grid_w*grid_h)*0.01;          // Number of stones - 10% of the screen 
const int predators=(grid_w*grid_h)*0.01;       // Number of predators - 10% of the screen  
const int preys=(grid_w*grid_h)*0.01;           // Number of preys - 10% of the screen  

const int preysLife=5;
const int preysReproduce=3;
const int predatorsLife=5;
const int predatorsHungryLife=4;
const int predatorsReproduce=3;

const int element_sz_x=canv_w/grid_w;           // Size X of one element
const int element_sz_y=canv_h/grid_h;           // Size Y of one element
const int maxX=canv_w;
const int minX=0;
const int maxY=canv_h;
const int minY=0;
const int centerX=(maxX+minX)/2;
const int centerY=(maxY+minY)/2;

GLfloat spin=0.0;
myOcean* oceanium;


void init(void)
{
    glClearColor(0.0,0.0,0.0,0.0);
    glShadeModel(GL_FLAT);
}

void drawGrid(void)
{
    glBegin(GL_LINES);
        glColor3f(0.5,0.5,0.5);
        for(int i=minX;i<=maxX;i+=element_sz_x)
        {
            glVertex3f(i,minY,0.0);
            glVertex3f(i,maxY,0.0);
        }
        for(int i=minY;i<=maxY;i+=element_sz_y)
        {
            glVertex3f(minX,i,0.0);
            glVertex3f(maxX,i,0.0);
        }
    glEnd();
}

void drawCoord(void)
{
    glBegin(GL_LINES);  // coordinate system
        glColor3f(0.0f,0.0f,1.0f); //z -axle
        glVertex3f(minX+10,minY+10,90.0);
        glVertex3f(minX+10,minY+10,-90.0);
        glColor3f(0.0f,1.0f,0.0f); //y -axle
        glVertex3f(minX+10,minY+10,90.0);
        glVertex3f(minX+10,maxY-10,90.0);
        glColor3f(1.0f,0.0f,0.0f); //x -axle
        glVertex3f(minX+10,minY+10,90.0);
        glVertex3f(maxX-10,minY+10,90.0);
    glEnd();
}

void drawFish(const int& x, const int& y)
{
    glBegin(GL_LINE_LOOP);   
        glColor3f(1.0f,1.0f,0.0f);
        glVertex3f(x+element_sz_x*0.1, y+element_sz_y*0.5, 0.0f);
        glVertex3f(x+element_sz_x*0.2, y+element_sz_y*0.5, 0.0f);
        glVertex3f(x+element_sz_x*0.3, y+element_sz_y*0.6, 0.0f);
        glVertex3f(x+element_sz_x*0.4, y+element_sz_y*0.6, 0.0f);
        glVertex3f(x+element_sz_x*0.5, y+element_sz_y*0.7, 0.0f);
        glVertex3f(x+element_sz_x*0.7, y+element_sz_y*0.5, 0.0f);
        glVertex3f(x+element_sz_x*0.8, y+element_sz_y*0.5, 0.0f);
        glVertex3f(x+element_sz_x*0.9, y+element_sz_y*0.6, 0.0f);
        glVertex3f(x+element_sz_x*0.9, y+element_sz_y*0.5, 0.0f);
        glVertex3f(x+element_sz_x*0.8, y+element_sz_y*0.4, 0.0f);
        glVertex3f(x+element_sz_x*0.9, y+element_sz_y*0.3, 0.0f);
        glVertex3f(x+element_sz_x*0.9, y+element_sz_y*0.2, 0.0f);
        glVertex3f(x+element_sz_x*0.8, y+element_sz_y*0.3, 0.0f);
        glVertex3f(x+element_sz_x*0.7, y+element_sz_y*0.3, 0.0f);
        glVertex3f(x+element_sz_x*0.6, y+element_sz_y*0.2, 0.0f);
        glVertex3f(x+element_sz_x*0.3, y+element_sz_y*0.2, 0.0f);
        glVertex3f(x+element_sz_x*0.2, y+element_sz_y*0.3, 0.0f);
        glVertex3f(x+element_sz_x*0.1, y+element_sz_y*0.3, 0.0f);
        glVertex3f(x+element_sz_x*0.2, y+element_sz_y*0.4, 0.0f);
    glEnd();
    
}

void display(void) 
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();


    //glRotatef(spin,0.0f,1.0f,0.0f);
    //glTranslated(centerX,0.0,0.0);
    //glScalef(1.0f,1.0f,1.0f);

    //gluLookAt(0.0,0.0,1.0,  0.0,0.0,0.0,  0.0,1.0,0.0);
    drawGrid();
    drawCoord();
    
    oceanium->print(element_sz_x,element_sz_y);
    oceanium->lifeStep();

 /*   glPushMatrix();
    glScalef(0.4f,0.4f,1.0f);
    glPopMatrix();*/


    glutSwapBuffers();
}

void spinDisplay(void)
{
    spin=spin+1.0;
    if(spin>360.0) spin=spin-360.0;
    glutPostRedisplay();
}

void reshape(int w, int h)
{
    glViewport(0,0,(GLsizei) w, (GLsizei) h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();       // reset view
    glOrtho(minX,maxX,minY,maxY,400,-400.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void mouse(int button,int state,int x,int y)
{
    switch(button)
    {
    case GLUT_LEFT_BUTTON:  if (state==GLUT_DOWN) glutIdleFunc(spinDisplay); break;
    case GLUT_RIGHT_BUTTON: if (state==GLUT_DOWN) glutIdleFunc(0); break;
    case GLUT_MIDDLE_BUTTON: if (state==GLUT_DOWN) glutIdleFunc(0); spin=0; oceanium->randAllocObj(); display(); break;
    }
}

void keyfun(unsigned char ch,int x,int y) 
{
    switch(ch)
    {
    case 27 :
    case 'q' : exit(0); break;
    default: spinDisplay();
    }
     
}

int main(int argc, char **argv)
{
    oceanium = new myOcean(grid_w,grid_h,stones,preys,predators);
    oceanium->setPreysLife(preysLife);
    oceanium->setPreysReproduce(preysReproduce);
    oceanium->setPredatorsLife(predatorsLife,predatorsHungryLife);
    oceanium->setPredatorsReproduce(predatorsReproduce);
    oceanium->randAllocObj();

    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB);
    glutInitWindowSize(canv_w,canv_h);
    glutInitWindowPosition(100,100);
    glutCreateWindow("������������� ����� ������");
    init();
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutMouseFunc(mouse);
    glutKeyboardFunc(keyfun);
    glutMainLoop();
    return 0;
}