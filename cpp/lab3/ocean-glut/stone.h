#ifndef _STONE_H_
#define _STONE_H_
#include"cell.h"

class Stone: public Cell
{
public:
    Stone(const int&,const int&);
    void draw(const int&,const int&) const;
};

#endif