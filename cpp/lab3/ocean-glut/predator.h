#ifndef _PREDATOR_H_
#define _PREDATOR_H_
#include"cell.h"

class Predator: public Cell
{
public:
    Predator(const int&,const int&);
    Predator(const int&,const int&,const int&,const int&,const int&);
    Predator(const Cell,const int&,const int&);
    Predator(const Cell*,const int&,const int&);
    void eat(const int&);
    void draw(const int&,const int&) const;
};

#endif