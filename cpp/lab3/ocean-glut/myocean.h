#ifndef _MYOCEAN_H_
#define _MYOCEAN_H_

#include"cell.h"


class myOcean
{
public:
    myOcean(const int&,const int&);
    myOcean(const int&,const int&,const int&,const int&,const int&);
    ~myOcean();

    void setStones(const int&);                         // ������������� ����� (default=0)

    void setPreys(const int&);                          // ������������� ������ (default=0)
    void setPreysLife(const int&);                      // ������������� ����� ����� ����� (default=10)
    void setPreysReproduce(const int&);                 // ������������� ����� ����������� ����� (default=3)

    void setPredators(const int&);                      // ������������� �������� (default=0)
    void setPredatorsLife(const int&,const int&);       // ������������� ����� ����� �������� (default=10) � ������ �� ������ (default=5)
    void setPredatorsReproduce(const int&);             // ������������� ����� ����������� �������� (default=3)

    void lifeStep(void);                                // ��������� ���
    void randAllocObj(void);                            // ������������ ������������� �������� �� �����
    void print(const int&,const int&) const;            // �����������


private: 
    void create(void);                                  // ������������� ������� ����������
    void destruct(void);                                // ������������ ������� ����������
    void preyStep(const int&,const int&);               // ��������� ��� ������
    void predatorStep(const int&,const int&);           // ��������� ��� �������
    bool checkDie(const int&,const int&);               // ���� ������ - ������� �� ������ ������
    bool selectFree(const int&,const int&,int&,int&);   // ���������� ���������� ������������ ��������� �������� ������
    bool selectPreyAround(const int&,const int&,int&,int&);   // ���������� ���������� ��������� ������
    void setUnused();                                   // ������ ������ ���������� ��� ������ ����� 
    
    Cell*** mapOcean;
    char** mapPrint;                                    // ����� ���������

    int img_w;
    int img_h;
    int stones;

    int predators;
    int predatorsLife;
    int predatorsActToDie;
    int predatorsReproduce;

    int preys;
    int preysLife;
    int preysReproduce;
};


#endif