#include"cell.h"
#include<iostream>
using namespace std;

Cell::Cell(const int& x,const int& y)
    :coord_x(x), coord_y(y), disp(' '),isFree(true),lifeTime(10),
        actionToDie(5),actionToReproduce(3),used(0)
{
}
Cell::Cell(const int& x,const int& y,const char& ds,const bool& bs)
    :coord_x(x), coord_y(y), disp(ds),isFree(bs),lifeTime(10),
        actionToDie(5),actionToReproduce(3),used(0)
{
}
Cell::Cell(const int& x,const int& y,const char& ds,const bool& bs,
    const int& life_t,const int& actToDie,const int& actToRepr)
    :coord_x(x), coord_y(y), disp(ds),isFree(bs),lifeTime(life_t),
        actionToDie(actToDie),actionToReproduce(actToRepr),used(0)
{
}

void Cell::draw(const int& sz_x,const int& sz_y) const
{
}

void Cell::print() const
{
    cout<<disp;
}

void Cell::setCounters()
{
    if(actionToDie>0) --actionToDie;
    if(actionToReproduce>0) --actionToReproduce;
    if(lifeTime>0) --lifeTime;
    used=1;
}


