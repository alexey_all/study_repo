#include"myocean.h"
#include<iostream>
#include<stdlib.h>
#include<time.h>

#include"predator.h"
#include"stone.h"
#include"prey.h"

using namespace std;

// public:
myOcean::myOcean(const int& x,const int& y)
{
    img_w=x;
    img_h=y;
    this->create();
    predators=preys=stones=0;
    predatorsLife=preysLife=10;
    predatorsActToDie=5;
    predatorsReproduce=preysReproduce=3;
}
myOcean::myOcean(const int& x,const int& y,const int& stone,const int& prey,const int& predator)
{
    img_w=x;
    img_h=y;
    this->create();
    predators=predator;
    preys=prey;
    stones=stone;
    predatorsLife=preysLife=10;
    predatorsActToDie=5;
    predatorsReproduce=preysReproduce=3;
    randAllocObj();
}
myOcean::~myOcean()
{
    this->destruct();
}


void myOcean::setStones(const int& cnt)         {if(cnt>=0) stones=cnt;}

void myOcean::setPreys(const int& cnt)          {if(cnt>=0)  preys=cnt;}
void myOcean::setPreysReproduce(const int& cnt) {if(cnt>=0) preysReproduce=cnt;}
void myOcean::setPreysLife(const int& life_t)   {if(life_t>=0) preysLife=life_t;}

void myOcean::setPredators(const int& cnt)      {if(cnt>=0) predators=cnt;}
void myOcean::setPredatorsReproduce(const int& cnt){if(cnt>=0) predatorsReproduce=cnt;}
void myOcean::setPredatorsLife(const int& life_t,const int& actToDie)
{
    predatorsLife=life_t;
    predatorsActToDie=actToDie;
}

void myOcean::randAllocObj(void)
{
    this->destruct();
    this->create();

    srand(time(0));
    int i,x,y;
    i=x=y=0;

    while(i<stones)                             // ����������� �����
    {
        x=rand()%img_w;
        y=rand()%img_h; 
        if(mapOcean[y][x]->free())
        {   
            delete mapOcean[y][x];
            mapOcean[y][x]=new Stone(x,y);
            i++;
        }
    }
    i=0;
    while(i<preys)                             // ����������� �����
    {
        x=rand()%img_w;
        y=rand()%img_h; 
        if(mapOcean[y][x]->free())
        {   
            delete mapOcean[y][x];
            mapOcean[y][x]=new Prey(x,y,preysLife,preysReproduce);
            i++;
        }
    }
    i=0;
    while(i<predators)                             // ����������� ��������
    {
        x=rand()%img_w;
        y=rand()%img_h; 
        if(mapOcean[y][x]->free())
        {   
            delete mapOcean[y][x];
            mapOcean[y][x]=new Predator(x,y,predatorsLife,predatorsActToDie,predatorsReproduce);
            i++;
        }
    }

}
void myOcean::lifeStep(void)
{
    Prey* tmpPrey = new Prey(0,0);          //��������� ����� ������� ��� ������ ����
    Predator* tmpPredator = new Predator(0,0);

    for(int i=0;i<this->img_h;i++)
    {
        for(int j=0;j<this->img_w;j++)
        {
            if(mapOcean[i][j]->getUsed()==false)  //���������� ������ ��������������� ������(����� �� ������� �����)
            {
                if(mapOcean[i][j]->type()==tmpPrey->type()) 
                    preyStep(j,i);
                else if(mapOcean[i][j]->type()==tmpPredator->type()) 
                    predatorStep(j,i);
            }
        }
    }

    setUnused();
    delete tmpPrey;
    delete tmpPredator;
}
void myOcean::print(void) const
{
    for(int i=0;i<this->img_h;i++)
    {
        for(int j=0;j<this->img_w;j++)
            mapPrint[i][j]=mapOcean[i][j]->type();
    }

    system("cls");
    for(int i=0;i<this->img_h;i++)
    {
        for(int j=0;j<this->img_w;j++)
            cout<<mapPrint[i][j];
        cout<<endl;
    }
}


// private:
void myOcean::create()
{
    mapOcean = new Cell**[this->img_h];
    mapPrint = new char*[this->img_h];

    for(int i=0;i<this->img_h;i++)
    {
        mapOcean[i]=new Cell*[this->img_w];
        mapPrint[i]=new char[this->img_w+1];
    } 
    for(int i=0;i<this->img_h;i++)
    {
        for(int j=0;j<this->img_w;j++)
            mapOcean[i][j]=new Cell(j,i);
    }
}
void myOcean::destruct()
{
    for(int i=0;i<this->img_h;i++)
    {
        for(int j=0;j<this->img_w;j++)
            delete mapOcean[i][j];
    }
    for(int i=0;i<this->img_h;i++)
    {
        delete [] mapOcean[i];
        delete [] mapPrint[i];
    } 
    delete [] mapOcean;
    delete [] mapPrint;
}

void myOcean::preyStep(const int& x,const int& y)
{
    int xi,yj;
    if(checkDie(x,y)) return; 
    if(mapOcean[y][x]->reproduce() && selectFree(x,y,xi,yj))
    {
        delete mapOcean[yj][xi];
        mapOcean[yj][xi]=new Prey(xi,yj,preysLife,preysReproduce);
        mapOcean[yj][xi]->setUsed(1);
        mapOcean[y][x]->setCounters();
    }
    else if(selectFree(x,y,xi,yj))
    {
        delete mapOcean[yj][xi];
        mapOcean[yj][xi]=new Prey(mapOcean[y][x]);
        mapOcean[yj][xi]->setCounters();
        delete mapOcean[y][x];
        mapOcean[y][x]=new Cell(x,y);
    }
}
void myOcean::predatorStep(const int& x,const int& y)
{
    int xi,yj;
    if(checkDie(x,y)) return;
    if(mapOcean[y][x]->reproduce() && selectFree(x,y,xi,yj))
    {
        delete mapOcean[yj][xi];
        mapOcean[yj][xi]=new Predator(x,y,predatorsLife,predatorsActToDie,predatorsReproduce);
        mapOcean[yj][xi]->setUsed(1);                
        mapOcean[y][x]->setCounters();
    }
    else if(selectPreyAround(x,y,xi,yj))
    {
        delete mapOcean[yj][xi];
        mapOcean[yj][xi]=new Predator(mapOcean[y][x]);
        mapOcean[yj][xi]->setActToDie(predatorsActToDie);
        mapOcean[yj][xi]->setCounters();
        delete mapOcean[y][x];
        mapOcean[y][x]=new Cell(x,y);
    }
    else if(selectFree(x,y,xi,yj))
    {
        delete mapOcean[yj][xi];
        mapOcean[yj][xi]=new Predator(mapOcean[y][x]);
        mapOcean[yj][xi]->setCounters();
        delete mapOcean[y][x];
        mapOcean[y][x]=new Cell(x,y);
    }

}
bool myOcean::checkDie(const int& x,const int& y)
{
    if(mapOcean[y][x]->die())
    {
        delete mapOcean[y][x];
        mapOcean[y][x]=new Cell(x,y);
        return true;
    }
    return false;
}
bool myOcean::selectFree(const int& x,const int& y,int& xi,int& yj)
{
    int i=x+rand()%3-1;
    int j=y+rand()%3-1;

    if(i>=0 && i<img_w && j>=0 && j<img_h)
    {
        if(mapOcean[j][i]->free())
        {
            xi=i;
            yj=j;
            return true;
        }
    }
    return false;
}
bool myOcean::selectPreyAround(const int& x,const int& y,int& xi,int& yj)
{
    Prey* tmpPrey = new Prey(0,0);
    for(int j=y-1;j<=y+1;j++)
    {
        for(int i=x-1;i<=x+1;i++)
        {
            if(i>=0 && i<img_w && j>=0 && j<img_h)
            {
                if(mapOcean[j][i]->type()==tmpPrey->type())
                {
                    xi=i;
                    yj=j;
                    return true;
                }
            }
        }     
    }
    delete tmpPrey;
    return false;
}
void myOcean::setUnused()
{
    for(int i=0;i<this->img_h;i++)
    {
        for(int j=0;j<this->img_w;j++)
            mapOcean[i][j]->setUsed(false);
    }
}
