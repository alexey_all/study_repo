#include"myocean.h"

int main()
{
    const int img_w=40;                         // ������ ��������
    const int img_h=15;                         // ������ ��������
    const int stones=(img_h*img_w)*0.01;         // ���������� ������ - 10% ������ 
    const int predators=(img_h*img_w)*0.01;      // ���������� �������� - 10% ������ 
    const int preys=(img_h*img_w)*0.01;          // ���������� ����� - 10% ������ 

    myOcean* oceanium = new myOcean(img_w,img_h,stones,preys,predators);    

    oceanium->setPreysLife(5);
    oceanium->setPreysReproduce(3);
    oceanium->setPredatorsLife(5,4);
    oceanium->setPredatorsReproduce(3);
    
    oceanium->randAllocObj();
    oceanium->print();

    while(1)
    {
        oceanium->lifeStep();
        oceanium->print();
    }

    return 0;
}
