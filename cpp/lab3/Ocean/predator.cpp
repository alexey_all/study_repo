#include"predator.h"
#include<iostream>
using namespace std;

Predator::Predator(const int& x,const int& y)
    :Cell(x,y,'S',false)
{
}

Predator::Predator(const int& x,const int& y,const int& life_t,
        const int& actToDie,const int& repr)
    :Cell(x,y,'S',false,life_t,actToDie,repr)
{
}

Predator::Predator(const Cell ptr)
    :Cell(ptr.getCoordX(),ptr.getCoordY(),'S',false,
        ptr.getLifeTime(),ptr.getActToDie(),ptr.getActToRepr())
{
}

Predator::Predator(const Cell* ptr)
    :Cell(ptr->getCoordX(),ptr->getCoordY(),'S',false,
        ptr->getLifeTime(),ptr->getActToDie(),ptr->getActToRepr())
{
}

void Predator::eat(const int& actToDie)
{
    Cell::setActToDie(actToDie);
}
