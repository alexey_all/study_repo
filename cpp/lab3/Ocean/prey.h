#ifndef _PREY_H_
#define _PREY_H_
#include"cell.h"

class Prey: public Cell
{
public:
    Prey(const int&,const int&);
    Prey(const int&,const int&,const int&,const int&);
    Prey(const Cell);
    Prey(const Cell*);
};

#endif