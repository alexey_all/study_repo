#include"prey.h"
#include<iostream>
using namespace std;

//public:
Prey::Prey(const int& x,const int& y)
    :Cell(x,y,'f',false)
{
}

Prey::Prey(const int& x,const int& y,const int& life_t,const int& repr)
    :Cell(x,y,'f',false,life_t,5,repr)
{
}

Prey::Prey(const Cell ptr)
    :Cell(ptr.getCoordX(),ptr.getCoordY(),'f',false,
        ptr.getLifeTime(),ptr.getActToDie(),ptr.getActToRepr())
{
}

Prey::Prey(const Cell* ptr)
    :Cell(ptr->getCoordX(),ptr->getCoordY(),'f',false,
        ptr->getLifeTime(),ptr->getActToDie(),ptr->getActToRepr())
{
}