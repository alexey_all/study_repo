#ifndef _CELL_H_
#define _CELL_H_

class Cell
{
public:
    Cell(const int&,const int&);
    Cell(const int&,const int&,const char&,const bool&);
    Cell(const int&,const int&,const char&,const bool&,const int&,const int&,const int&);

    virtual void print();
    void setCounters(void);   // ������������� ��-� ��������� ����� ����
    void setUsed(const bool& usd)       {this->used=usd;}
    void setActToDie(const int& cnt)    {if(cnt>=0) actionToDie=cnt;}

    virtual char type() const           {return disp;}
    virtual bool free() const           {return isFree;}
    virtual bool die() const            {return (lifeTime<=0 || actionToDie<=0)?true:false;}
    virtual bool reproduce()            {return (actionToReproduce<=0)?true:false;}
    virtual bool getUsed() const        {return used;}
    virtual int getCoordX() const       {return coord_x;}
    virtual int getCoordY() const       {return coord_y;} 
    virtual int getActToDie() const     {return actionToDie;}
    virtual int getActToRepr() const    {return actionToReproduce;}
    virtual int getLifeTime() const     {return lifeTime;}

    
private:
    bool used;
    char disp;
    bool isFree;
    int coord_x;  
    int coord_y;
    int actionToDie;
    int actionToReproduce;
    int lifeTime;
};
#endif