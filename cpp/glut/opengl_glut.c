#include <gl/glut.h>   //����������� ���������� glut.h
 
GLfloat spin=0.0;
void Initialize()
{
glClearColor(0.0,0.0,0.0,1.0);      //������� ������� (���������) ����
glMatrixMode(GL_PROJECTION);        //���������� ��������
glLoadIdentity();                   // 
glOrtho(0.0,1.0,0.0,1.0,-1.0,1.0);  // ������� ���������
}

void spinDisplay(void)
{
spin=spin+1.0;
if(spin>360.0) spin=spin-360.0;
glutPostRedisplay();
}

void draw()
{
//������� ����� 
glClear(GL_COLOR_BUFFER_BIT);
glPushMatrix();
glRotatef(spin,0.0,spin,1.0); 
//��������� �������� 
glColor3f(1.0,1.0,1.0);    //�������� ����� ���� ��� ���������
glBegin(GL_POLYGON);       //������ ������
glVertex3f(0.25,0.25,0.0); //���������� ��������
glVertex3f(0.75,0.25,0.0); 
glVertex3f(0.75,0.75,0.0);
glVertex3f(0.25,0.75,0.0);
glEnd();

glutWireSphere(0.5,30,30);
glPopMatrix();
glFlush();                  // �������� ����
}
 
//����� � ������� ����
int main(int argc, char **argv)
{
glutInit(&argc,argv);
glutInitDisplayMode(GLUT_SINGLE|GLUT_RGB);
glutInitWindowSize(400,400);		//��������� ������ ����
glutInitWindowPosition(100,100);	//������� ����
glutCreateWindow("Polygon");		//��� ����
Initialize();						//����� ������� Initialize
glutDisplayFunc(draw);				//����� ������� ���������
//glutReshapeFunc(reshape);
glutIdleFunc(spinDisplay);
glutMainLoop();
 
return 0;
}