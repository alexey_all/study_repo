#ifndef _PHONEBOOK_H_
#define _PHONEBOOK_H_
#include"record.h"
    
class Phonebook
{
public:
    Phonebook();
    ~Phonebook();

    void add(const char*,const char*,const char*);
    void del(const char*);
    void del(const int&);
    int find(const char*) const;
    void print(void) const;
    void print(const int&) const;
    void print(const char*) const;

private:
    Record* book;
    int count;
};

#endif