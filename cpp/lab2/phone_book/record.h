#ifndef _RECORD_H_
#define _RECORD_H_

class Record
{
public:
    Record();
    Record(char*,char*,char*);
    Record::Record(const Record&);
    ~Record();
    const Record& operator=(const Record&);
    void set(const char*,const char*,const char*);
    void setName(const char*); // �� ������ ��-� �������� ���-��
    void setPhone(const char*);
    void setAdress(const char*);
    void getName(char*) const; // �� ������ ���������� ���-��
    void getPhone(char*) const;
    void getAdress(char*) const;

private:
    char* name;
    char* phone;
    char* adress;
};

#endif