#include<iostream>
#include"record.h"
#define Nb 50
#define Ns 20
using namespace std;


Record::Record()
{
    name = new char[Nb];
    phone = new char[Ns];
    adress = new char[Nb];
    name[0]=phone[0]=adress[0]='\0';
}

Record::Record(char* n,char* p,char* a = "")
{
    name = new char[Nb];
    phone = new char[Ns];
    adress = new char[Nb];
    this->set(n,p,a);
}

Record::Record(const Record& res)
{
    name = new char[Nb];
    phone = new char[Ns];
    adress = new char[Nb];
    this->set(res.name,res.phone,res.adress);
}

Record::~Record()
{
    delete [] name;
    delete [] phone;
    delete [] adress;
}

const Record& Record::operator=(const Record& res)
{
    if(&res!=this)
        this->set(res.name,res.phone,res.adress);
    return *this;
}

void Record::set(const char* n,const char* p,const char* a)
{
    strncpy(name,n,Nb-1);
    strncpy(phone,p,Ns-1);
    strncpy(adress,a,Nb-1);
}

void Record::setName(const char* n)
{
    strncpy(name,n,Nb-1);
}

void Record::setPhone(const char* p)
{
    strncpy(phone,p,Ns-1);
}

void Record::setAdress(const char* a)
{
    strncpy(adress,a,Nb-1);
}

void Record::getName(char* n) const
{
    strncpy(n,name,Nb);   
}

void Record::getPhone(char* p) const
{
    strncpy(p,phone,Ns);
}

void Record::getAdress(char* a) const
{
    strncpy(a,adress,Nb);
}