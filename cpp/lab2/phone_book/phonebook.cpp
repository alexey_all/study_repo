#include<iostream>
#include"phonebook.h"
//#include<stdlib.h>
using namespace std;


Phonebook::Phonebook()
    : book(NULL), count(0)
{
}

Phonebook::~Phonebook()
{
    delete [] book;
}

void Phonebook::add(const char* name,const char* phone,const char* adress)
{
    Record* tmp = new Record[count+1];
    for(int i=0; i<count; i++)
        tmp[i]=book[i];
    tmp[count++].set(name,phone,adress);
    if(book!=NULL)
        delete [] book;
    book=tmp;
}

void Phonebook::del(const char* name)
{
    int i = this->find(name);
    this->del(i);
}

void Phonebook::del(const int& i)
{
    if(i>=0)
    {
        Record* tmp = new Record[--count];
        for(int j=0; j<i; j++)
            tmp[j]=book[j];
        for(int j=i; j<count; j++)
            tmp[j]=book[j+1];
        delete [] book;
        book=tmp;
    }
}

int Phonebook::find(const char* name) const
{
    char t[50]="\0";
    for(int i=0; i<count; i++)
    {
        book[i].getName(t);
        if(strcmp(t,name)==0)
            return i;
    }
    return -1;
}

void Phonebook::print(void) const
{
    for(int i=0; i<count; i++)
        this->print(i);
}

void Phonebook::print(const int& i)const
{
    char t[50]="\0";
    book[i].getName(t);
    cout<<i<<"\t"<<t<<"\t";
    book[i].getPhone(t);
    cout<<t<<"\t";
    book[i].getAdress(t);
    cout<<t<<endl;
}

void Phonebook::print(const char* name)const
{
    int i = this->find(name);
    if(i>=0)
        this->print(i);
}