#include<iostream>
#include"mystring.h"
using namespace std;

int main()
{
    Mystring *str1 = new Mystring;
    Mystring *str2 = new Mystring('s');
    Mystring *str3 = new Mystring("third string");

    *str1=*str3;
    *str1=*str2+*str3;
    *str3+=*str1;
    *str3+="test add";

    str1->print();
    str2->print();
    str3->print();

    delete str1;
    delete str2;
    delete str3;
    return 0;
}