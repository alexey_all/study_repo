#ifndef _MYSTRING_H_
#define _MYSTRING_H_

class Mystring
{
public:
    Mystring();                 // конструктор по умолчанию
    Mystring(char);               
    Mystring(char*);        
    Mystring(const Mystring&);  // копирующий конструктор
    ~Mystring();

    const Mystring& operator= (const Mystring&);
    Mystring operator+ (const Mystring&);
    Mystring& operator+= (const Mystring&);
    
    void print() const;
    int len() const;
private:
    char* string;
};

#endif