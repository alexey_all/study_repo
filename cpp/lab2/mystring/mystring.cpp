#include<iostream>
#include"mystring.h"
using namespace std;

Mystring::Mystring()
{
    string = new char;
    *string='\0';
    cout<<"free constructor "<<endl;
} 

Mystring::Mystring(char inp)
{
    string = new char[2];
    string[0]=inp;
    string[1]='\0';
    cout<<"constructor char "<<inp<<endl;
}

Mystring::Mystring(char* inp)
{
    string = new char[strlen(inp)+1];
    strcpy(string,inp);
    cout<<"constructor char* "<<inp<<endl;
} 

Mystring::Mystring(const Mystring& res)
{
    string = new char[res.len()+1];
    strcpy(string,res.string);
    cout<<"constructor Mystring "<<res.string<<endl;
}

Mystring::~Mystring()
{
        cout<<"destructor "<<this->string<<endl;
    delete [] this->string;
}

const Mystring& Mystring::operator= (const Mystring& res)
{
    if(&res!=this)
    {
        delete [] string;
        string= new char[res.len()+1];
        strcpy(string,res.string);
        return *this;
    }
}

Mystring Mystring::operator+ (const Mystring& res)
{
    char* tmpc = new char[this->len()+res.len()+1];
    strcpy(tmpc,this->string);
    strcat(tmpc,res.string);
    Mystring tmp(tmpc);
    delete [] tmpc;
    return tmp;
}

Mystring& Mystring::operator+= (const Mystring& res)
{
    char* tmpc = new char[this->len()+1];
    strcpy(tmpc,this->string);
    delete [] this->string;
    this->string=new char[this->len()+res.len()+1];
    strcpy(this->string,tmpc);
    strcat(this->string,res.string);
    return *this;
}

void Mystring::print() const
{
    cout<<string<<endl;
}

int Mystring::len() const
{
    return strlen(string);
}