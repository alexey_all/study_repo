#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<time.h>

#define INP_FILE "text.txt"
#define MAX_LENGHT 1024

int printfile(char *fname);
void parseWords(char *str);
void mixLetters(char *start, int lenght);
void clearArr(char *arr,int lenght);

int main()
{
    int err=printfile(INP_FILE);

    if(err!=0)
    {
        puts("Error opening input file");
        return -1;
    }
    return 0;
}

int printfile(char *fname)
{
    char str[MAX_LENGHT];
    FILE *fl;

    if((fl=fopen(fname,"r"))==0)
        return -1;

    while(!feof(fl))  //read file before EOF
    {
        clearArr(str, MAX_LENGHT);
        fgets(str,MAX_LENGHT,fl);
        if(str[strlen(str)-1]=='\n')
            str[strlen(str)-1]='\0';
        parseWords(str);
        puts(str);
    }
   
    fclose(fl);
    return 0;
}

void parseWords( char *str)
{
    int inWord=0,len=0;
    char *start=str;

    while(1)
    {
        if(*str=='\0')
        {
            mixLetters(start,len);
            len=0;
            break;
        }
        else   if(*str!=' ' && inWord==0)
        {
            start=str;
            len++;
            inWord=1; 
        }
        else   if(*str!=' ' && inWord==1)
            len++;
        else if(*str==' ' && inWord==1)
        {
            inWord=0;
            mixLetters(start,len);
            len=0;
        }   
        str++;
    }
}

void mixLetters(char *start, int lenght)
{
    char tmp;
    int i=1,random=0;

    lenght-=2; // not take first and last element of array
    
    srand(time(0));

   // while(start+i<start+((lenght<3)?'0':lenght))
    while(start+i<start+lenght) // if 3 elements in array nothing to do
    {
        random=rand()%lenght+1;
        
        tmp=*(start+i);
        *(start+i)=*(start+random);
        *(start+random)=tmp;

        i++;
    }
}

void clearArr(char *arr,int lenght)
{
    int j=0;
    while(j<lenght)
        arr[j++]='\0';   
}