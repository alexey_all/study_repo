#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<time.h>

#define X 40
#define Y 30

void initQuadrant(char* (*p)[X+1]);
void copyQuadrant(char* (*p)[X+1]);
void print(char (*p)[X+1]);
void clrscr(void);
void sleep(void);

int main()
{
    while(1)
    {
        char arr[Y][X+1]={0};
        initQuadrant(arr);
        copyQuadrant(arr);
        clrscr();
        print(arr);
        sleep();
    }
    return 0;
}

void initQuadrant(char (*p)[X+1])
/* ������� ��������� ������� ����� �������� ������� �������
   �������� �� ��������� ��������� ��������� � 2�������� ������� */
{
    int maxX=X/2;
    int maxY=Y/2;
    int x=0,y=0;

    srand(time(0));

    while(y<maxY)
    {
        x=0;
        while(x<maxX)
        {
            *(p[y]+x)=rand()%4?' ':'*';
            x++;
        }
        *(p[y]+x)='\0';
        y++; //��� �����, ���� ������� p++
    }
}

void copyQuadrant(char (*p)[X+1])
/* ������� �������� �������� ������ �������� ������� � ������ ��� */
{
    int maxX=X/2;
    int maxY=Y/2;
    int x=0,y=0;

    while(y<maxY)
    {
        x=0;
        while(x<maxX)
        {
            *(p[Y-y-1]+X-x-1)=*(p[Y-y-1]+x)=*(p[y]+X-x-1)=*(p[y]+x);
            x++;
        }
        y++;
    }

 
    
}

void print(char (*p)[X+1])
{
    int y=0;

    while(y<Y)
    {
        puts(p[y]);
        y++;
    }

}

void clrscr(void)
{
    system("cls");
}

void sleep(void)
{
    clock_t begin=clock();
    while(clock()<begin+CLOCKS_PER_SEC);
}


