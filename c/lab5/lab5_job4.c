#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<time.h>

#define MAX_LENGHT 256
#define INP_FNAME "text.txt"

int getWords(char *str,char *p[]);
int printRandString(char *string);
int readFile(char *fname);
void printWord(char *p); 

int main()
{
    srand(time(0));

    if(readFile(INP_FNAME)!=0)
    {
        puts("Can not open input file") ;
        return -1;
    }

    return 0;
}

int getWords(char *str,char *p[])
/* Function add to *p[] start addresses of words in string 
   Return count words */
{
    int inWord=0,i=0,count=0;

    while(*str)
    {
        if(*str!=' ' && inWord==0)
        {  
            inWord=1;
            count++;
            if(i<MAX_LENGHT/8)
                p[i++]=str;    
        }
        else if(*str==' ' && inWord==1)
            inWord=0;
        str++;
    }

    return count;
}

void printWord(char *p) 
/* print only one word */ 
{
    while(*p!=' ' && *p!= '\0')
        putchar(*(p++));
}

int printRandString(char *string)
/* Receive words from string and print */
{
    char *p[MAX_LENGHT/8]={0},*tmp;
    int i=0;
    int count=getWords(string, p);

    if(count<0)         // if function getWords has error
        return -1;
    else if(count==0)    // if empty string
        putchar('\n');

    while(count>0)
    {
        i=rand()%count;
       
        printWord(p[i]);
        if(count>1)
            putchar(' ');
        else
            putchar('\n');

        tmp=p[i];
        p[i]=p[count-1];
        p[count-1]=tmp;    
        
        count--;
    }

    return 0;
}

int readFile(char *fname)
/* read file and line-print */
{
    char str[MAX_LENGHT]={'\0'};
    FILE *fl;

    if((fl=fopen(fname,"r"))==0)
        return -1;

    while(!feof(fl))  //read file before EOF
    {
        fgets(str,MAX_LENGHT,fl);
        if(str[strlen(str)-1]=='\n')
            str[strlen(str)-1]='\0';
        if (printRandString(str)!=0)
            return -1;
    }
   
    fclose(fl);
    return 0;
}

