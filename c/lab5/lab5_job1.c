#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<time.h>

#define MAX_LENGHT 256

int getWords(char *str,char *p[]);
int printRandString(char *string);
void printWord(char *p); 

int main()
{
    char string[MAX_LENGHT]={'\0'};

    printf("Enter a phrase\n");
    fgets(string,MAX_LENGHT,stdin);
    string[strlen(string)-1]='\0';

    srand(time(0));

    printRandString(string);

    return 0;
}

int getWords(char *str,char *p[])
/* Function add to *p[] start addresses of words in string 
   Return count words */
{
    int inWord=0,i=0,count=0;

    while(*str)
    {
        if(*str!=' ' && inWord==0)
        {  
            inWord=1;
            count++;
            if(i<MAX_LENGHT/8)
                p[i++]=str;    
        }
        else if(*str==' ' && inWord==1)
            inWord=0;
        str++;
    }

    return count;
}

void printWord(char *p) 
/* print only one word */ 
{
    while(*p!=' ' && *p!= '\0')
        putchar(*(p++));
}

int printRandString(char *string)
/* Receive words from string and print */
{
    char *p[MAX_LENGHT/8]={0},*tmp;
    int i=0;
    int count=getWords(string, p);

    if(count<0)         // if function getWords has error
        return -1;
    else if(count==0)    // if empty string
        putchar('\n');

    while(count>0)
    {
        i=rand()%count;
       
        printWord(p[i]);
        if(count>1)
            putchar(' ');
        else
            putchar('\n');

        tmp=p[i];
        p[i]=p[count-1];
        p[count-1]=tmp;    
        
        count--;
    }

    return 0;
}

