#include<stdio.h>
#include<string.h>
#include<stdlib.h>

struct WORD
{
    char name[20];
    struct WORD* left;
    struct WORD* right;
    int count;
};

struct WORD* buildTree(struct WORD * root, char* name)
{
    if(root==NULL)
    {
        root=(struct WORD*)malloc(sizeof(struct WORD));
        root->left=root->right=NULL;
        strcpy(root->name,name);
        root->count=0;
    }
    else if(strcmp(root->name,name)>0)
        root->left=buildTree(root->left, name);
    else if(strcmp(root->name,name)<0)
        root->right=buildTree(root->right, name);  

    return root;
};

struct WORD* selectMax(struct WORD * root)
{
    struct WORD* right=NULL;
    struct WORD* left=NULL;
    struct WORD* max=root;

    if(root->left)
    {
        left=selectMax(root->left);
        if(max->count<left->count)
            max=left;
    }
    if(root->right)
    {
        right=selectMax(root->right);
        if(max->count<right->count)
            max=right;
    }
    return max;
};

void printTree(struct WORD * root)
{
    struct WORD* max=NULL;

    while(1)
    {
        max=selectMax(root);
        if(max->count==0)
            break;
        printf("%s - %d\n",max->name,max->count);
        max->count=0;
    }
/*
    if(root->left)
        printTree(root->left);
    printf("%s - %d\n",root->name,root->count);
    if(root->right)
        printTree(root->right);
*/

};

void incrementTree(struct WORD * root, char* name)
{
    if(root->left)
        incrementTree(root->left,name);

    if(strcmp(root->name,name)==0)
        root->count++;

    if(root->right)
        incrementTree(root->right,name);
};

void separate(struct WORD * root,char *str)
{
    int inWord=0,i=0,count=0;
    char *ptr=str;
    char buf[30];

    while(*ptr)
    {
        if(strchr(" ;:'\"\\,.[]{}<>*&^%$#@!/()", *ptr)) //removing unnecessary characters
            *ptr=' ';
        ptr++;
    }
    ptr=str;

    while(1)
    {
        if(*str=='\0')
        {
            strncpy(buf,ptr,count);
            buf[count]='\0';
            incrementTree(root,buf);
            break;
        }
        if(inWord==0 && *str!=' ')
        {
            ptr=str;
            inWord=1;
            count++;
        }
        else if(inWord==1 && *str!=' ')
            count++;
        else if(inWord==1 && *str==' ')
        {
            strncpy(buf,ptr,count);
            buf[count]='\0';
            incrementTree(root,buf);
            inWord=0;
            count=0;
        }
        str++;
    }
};

int main()
{
    struct WORD* root=NULL;
    FILE *fp=fopen("names.txt","r");
    FILE *fin=fopen("lab7_job1.c","r");
    char buf[256];
    
    if(fp==NULL || fin==NULL)
    {
        perror("File error: ");
        return -1;
    }

    while(fgets(buf,256,fp))
    {   
        if(buf[strlen(buf)-1]=='\n')
            buf[strlen(buf)-1]='\0';
        root=buildTree(root,buf);
    }

    while(fgets(buf,256,fin))
        separate(root,buf);

    printTree(root);

    fclose(fp);
    fclose(fin);
    return 0;
}
