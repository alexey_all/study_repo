#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef long UL;

int fibonacciRec(int number)
{
    if(number==1)
        return 1;
    if(number<1)
        return 0;
    return fibonacciRec(number-1)+fibonacciRec(number-2);
}

int main()
{
    int number,i=0;
    clock_t time_s,time_e;
    char str[50];
    FILE *file_out;
    
    if ((file_out = fopen("lab6_job5_output.txt", "w"))==0) 
            return -1;

    while(i<=40)
    {
        time_s=clock();
        number=fibonacciRec(i);
        time_e=clock();
        sprintf(str, "%d,%d,%d\n",i,number,time_e-time_s); 
        printf("%d is %d, time is %d\n",i++,number,time_e-time_s);
        fputs(str,file_out);  //write to new file
    }

    fclose(file_out);
    return 0;
}