#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef long UL;

UL pow(int x, int n) {
    UL sum=0;
    if(n==0) 
        return 1;
    sum=x*pow(x, n-1);
    return sum;
 }

void initRand(int *arr,UL size)
{
    UL i=0;
    srand(time(0));
    for(i;i<size; i++)
        arr[i]=rand()%10;
}

UL sum(int *arr,UL size)
{
    UL sum=0, i=0;
    while(i<size)
        sum+=arr[i++];
    return sum;
}

UL sumRec(int *arr,UL size)
{
    if(size<=1)
        return arr[0];
    return (sumRec(arr, size/2)+sumRec(&arr[size/2], size/2));
}

void checkTime(UL (*fun)(int*,UL),int *arr,UL size,char* fName)
{
    UL sum;
    clock_t time_s,time_e;
    
    time_s=clock();
    sum=fun(arr,size);
    time_e=clock();

    printf("%s is %d, time is %d\n",fName,sum,time_e-time_s);
}

int main(int argc, char *argv[])
{
    int exp;
    UL size;
    int *arr=0;
    
    if(argc<2)
    {
        printf("Error! You must input size of array in command string\n");
        return -1;
    }

    exp=atoi(argv[1]);
    size=pow(2,exp);

    if(exp>20 || exp<1)
    {
        printf("Error! Exponent must in 1 to 20\n");
        return -1;
    }

    arr=(int*)malloc(size*sizeof(int));

    if(!arr)
    {
        puts("Memory allocation error!");
        return -2;
    }

    initRand(arr,size);
 
    checkTime(sum,arr,size,"Cyclic sum");
    checkTime(sumRec,arr,size,"Recursive sum");

    free(arr);
    return 0;
}