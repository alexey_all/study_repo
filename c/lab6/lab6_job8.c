#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define SIZE 256

char partition(char *buf, char *expr1, char *expr2)
{
    int begin=0,end=0,pos=1,i,j;
    char action;

    while(1)  //find end of the big expression
    {
        if(*(buf+pos)=='(')
            begin++;
        if(*(buf+pos)==')')
            end++;
        if(begin==end)
            break;
        pos++;
    }

    for(i=1;i<=pos;i++)             //init first expression
        expr1[i-1]=buf[i];
    expr1[i-1]='\0';

    action=buf[++pos]; 

    for(j=++pos,i=0;buf[j]!='\0';j++) //init second expression
        expr2[i++]=buf[j];
    expr2[j]='\0';
    return action;
}

int eval(char *buf)
{
    char expr1[SIZE/2],expr2[SIZE/2],action;

    if(*buf!='(') 
        return atoi(buf);

    action=partition(buf,expr1,expr2); 

    switch(action) 
    {
        case '+':
            return eval(expr1)+eval(expr2); 
        case '-':
            return eval(expr1)-eval(expr2);
        case '*':
            return eval(expr1)*eval(expr2); 
        case '/':
            return eval(expr1)/eval(expr2);
    }

    return 0;
}

int main(int arvc,char *argv[])
{   
    char *buf=argv[1];
    buf[strlen(buf)-1]='\0';

    printf("%d\n", eval(buf));
    return 0;
}