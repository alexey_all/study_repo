#include <stdio.h>

#define SIZE_X 150
#define SIZE_Y 70

int crossRec(int X, int Y, int offset,char (*arr)[SIZE_X+1]);

int main()
{
    int i=0,j=0;
    char arr[SIZE_Y][SIZE_X+1]={'\0'};
    
    //initialization array of spaces;
    while(i<SIZE_Y)
    {
        j=0;
        while(j<SIZE_X)
            arr[i][j++]=' ';
        i++;
    }

    crossRec(SIZE_X/2, SIZE_Y/2, 8, arr);

    // print array 
    i=0;
    while(i<SIZE_Y)
        puts(arr[i++]);

    return 0;
}

int crossRec(int X, int Y, int offset,char (*arr)[SIZE_X+1])
{
    *(arr[Y]+X)='*';
    *(arr[Y+offset+1]+X)=*(arr[Y-offset-1]+X)=*(arr[Y]+X+offset+1)=*(arr[Y]+X-offset-1)='*';

    if(offset<=1)
    {
        return -1;
    }

    crossRec(X, Y, offset/3, arr);
    crossRec(X+offset+1, Y, offset/3, arr);
    crossRec(X-offset-1, Y, offset/3, arr);
    crossRec(X, Y+offset+1, offset/3, arr);
    crossRec(X, Y-offset-1, offset/3, arr);

    return 0;
}