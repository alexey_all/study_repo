#include <stdio.h>
#include <stdlib.h>

#define SIZE_X 28
#define SIZE_Y 10

void printPicture(char (*arr)[SIZE_X+1])
{
    int x=0,y=0;

    system("cls");

    while(y<SIZE_Y)
    {
        x=0;
        while(x<=SIZE_X)
             putchar(*(arr[y]+x++));
        putchar('\n');
        y++;
    }
}

int findOut(char (*arr)[SIZE_X+1],int x,int y,int lastx,int lasty)
{

    *(arr[lasty]+lastx)='.';
    *(arr[y]+x)='x';
    
    printPicture(arr); //print picture

    if(x<=0 || x>=SIZE_X-1 || y<=0 || y>=SIZE_Y) //go out from maze
        return 1;

    if(checkFree(arr,x,y)==1)
        return 1;

    // go back
    *(arr[lasty]+lastx)='x';
    *(arr[y]+x)='.';
    printPicture(arr); //print picture

    return 0;
}

int checkFree(char (*arr)[SIZE_X+1],int x,int y)
{ //Function start check all direction from start point
    if(*(arr[y]+x+1)==' ')
        if(findOut(arr,x+1,y,x,y)==1) //exit if already finded out 
            return 1;
    if(*(arr[y+1]+x)==' ')
        if(findOut(arr,x,y+1,x,y)==1)
            return 1;
    if(*(arr[y]+x-1)==' ')
        if(findOut(arr,x-1,y,x,y)==1)
            return 1;
    if(*(arr[y-1]+x)==' ')
        if(findOut(arr,x,y-1,x,y)==1)
            return 1;
    return 0;
}

int main()
{
    char arr[SIZE_Y][SIZE_X+1]={
        {"############################"},
        {"#                 #        #"},
        {"# ###############          #"},
        {"#           #   #######  ###"},
        {"# ######    #               "},
        {"#      #    #   #######   ##"},
        {"#####  #    #   #         ##"},
        {"#      #        #     ######"},
        {"############################"}
    };

    checkFree(arr,SIZE_X/2,SIZE_Y/2);
    
    return 0;
}