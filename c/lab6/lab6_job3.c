#include <stdio.h>

#define SIZE 10

int intToCharRec(int val,char* arr, int iteration)
{
    int i=0;
    char tmp;

    if(val<1)
    {
        //shift all array and exit
        while(iteration>=0)
        {
            tmp=arr[i];
            arr[i]=arr[SIZE-iteration];
            arr[SIZE-iteration]=tmp;
            i++;
            iteration--;
        } 
        return -1;
    }

    //add last digit to array and recursion
    arr[SIZE-(++iteration)]=val%10+'0';
    intToCharRec(val/10,arr,iteration);
    return 0;
}

int main()
{
    int val;
    char arr[SIZE+1]={'\0'};

    puts("input the number");
    if(scanf("%d",&val)!=1)
        return -1;

    intToCharRec(val,arr,0);
    puts(arr);
    return 0;
}