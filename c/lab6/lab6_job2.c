#include <stdio.h>


int kollatcRec(unsigned int num, int iteration)
{
    if(num<2)
        return iteration;
    else
    {
        ++iteration;
        if(num%2)
            return kollatcRec(3*num+1, ++iteration);
        else
            return kollatcRec(num/2, ++iteration);
    }
}

int main()
{
    unsigned int i,maxlen=0,maxnum=0;
   // printf("%d = %d\n",113383, kollatcRec(113383, 0));
   // return 0;
    for(i=2;i<10;i++)
    {
        printf("%d = %d\n",i, kollatcRec(i, 0));
    }
    return 0;
}