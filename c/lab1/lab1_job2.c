﻿#include<stdio.h>

int main()
{
	int hour;
	int min;
	int sek;

	printf("Enter time in format HH:MM:SS\n");
	
	if(scanf("%d:%d:%d",&hour,&min,&sek)!=3)
	{
		printf("Entered incorrect data\n");
		return -1;
	}

	if((hour<0 || hour>23) || (min<0 || min>59) || (sek<0 || sek>59))
	{
		printf("Entered incorrect data\n");
		return -1;
	}

	
		
	if(hour>=6 && hour<9)
		printf("Good morning\n");
	else if(hour>=9 && hour<18)
		printf("Good day\n");
	else if(hour>=18 && hour<21)
		printf("Good evening\n");
	else
		printf("Good night\n");

	return 0;
}