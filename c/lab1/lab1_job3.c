﻿#include<stdio.h>

int main()
{
	float angle;
	char measure;
	float koeff=57.3;

	printf("Enter angle: ");
	
	if(scanf("%f%c:",&angle,&measure)!=2)
	{
		printf("Entered incorrect data\n");
		return -1;
	}
	
	if(measure=='D')
		printf("%.3fR\n",angle/koeff);
	else if(measure=='R')
		printf("%.3fD\n",angle*koeff);
	else 
	{
		printf("Entered incorrect data\n");
		return -1;
	}

	return 0;
}