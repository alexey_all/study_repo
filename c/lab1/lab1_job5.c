#include<stdio.h>
#include<string.h>

int main()
{
	char words[256];
        char attr[10];
	int indent;

	printf("Input phrase:\n");
	scanf("%s",words);
	
	indent=(80-strlen(words))/2+strlen(words);

        sprintf(attr, "%%%ds\n", indent);
        printf(attr,words);
        
	return 0;
}