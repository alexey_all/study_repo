﻿#include<stdio.h>

int main()
{
	int ft;
	int inch;
	
	printf("Enter height in ft and inch:\n");
	
	if(scanf("%d%d:",&ft,&inch)!=2)
	{
		printf("Entered incorrect data\n");
		return -1;
	}
	
	printf("Your height is %.1f sentimetr\n", (float)(ft*12+inch)*2.54);
	

	return 0;
}