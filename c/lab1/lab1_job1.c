#include<stdio.h>

int main()
{
	
	int height;
	int weight;
	int koeff;  // коэффициент идеального веса
	int result; // идеальный рассчетный вес
	char sex;

	printf("Please, enter your sex (M-male or F-female): ");
	scanf("%c",&sex);

	if(sex=='M')
		koeff=100;
	else if(sex=='F')
		koeff=110;
	else
	{
		printf("You have entered the wrong data\n");
		return -1;
	}

	printf("Enter your height in cm: ");
	if(scanf("%d",&height)!=1)
	{
		printf("You have entered the wrong data\n");
		return -1;
	}

	printf("Enter your weight in kg: ");
	if(scanf("%d",&weight)!=1)
	{
		printf("You have entered the wrong data\n");
		return -1;
	}

	result=height-koeff;

	if(result>weight+5)
		printf("Your weight is too little\n");
	else if(result<weight-5)
		printf("Your weight is too big\n");
	else 
		printf("All is well, your weight is normal\n");

	return 0;
}