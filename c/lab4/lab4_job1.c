#include<stdio.h>
#include<string.h>

#define SIZE 256
int main()
{
        char arrStr[SIZE][SIZE];
        char *pStr[SIZE],*tmp;
        int endStr=0,i=0,x=0;
        
        printf("Enter a phrase or press 'Enter' to end\n");
        
        do              //input strings to array
        {
            fgets(arrStr[endStr],SIZE,stdin);
            arrStr[endStr][strlen(arrStr[endStr])-1]='\0';
            pStr[endStr]=arrStr[endStr];
        }
        while(arrStr[endStr++][0]!='\0');

        endStr-=2;
        
        while(i<=endStr) //sorting array of pointers
        {
            x=endStr;
            while(i<x) 
            {
                if(strlen(pStr[i])>strlen(pStr[x]))
                {
                    tmp=pStr[x];
                    pStr[x]=pStr[i];
                    pStr[i]=tmp;
                }
                x--;
            }
            puts(pStr[i]);
            i++;
        }   
         
	return 0;
}