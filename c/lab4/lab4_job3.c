#include<stdio.h>
#include<string.h>

#define SIZE 256

int main()
{
        char string[SIZE];
        char *pStr[SIZE],*start,*end;
        int pal=1;
        
        printf("Enter a phrase\n");
        fgets(string,SIZE,stdin);
        string[strlen(string)-1]='\0';
      
	start=string;
	end=&string[strlen(string)-1];

        while(start<=end)
        {
	    if(*start!=*end)
		pal=0;
	    start++;
	    end--;
        }   

	if(pal)
	    puts("This string is a palindrom");
	else
	    puts("This string is NOT a palindrom");

	return 0;
}