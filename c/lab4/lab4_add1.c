/* This program print only reseived strings */
/* Format input: 10-20,22,25-31             */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#define MAX_SIZE 1024
#define INPUT "./lab4_add1.c"


void separation(char str[MAX_SIZE], int Num[MAX_SIZE*MAX_SIZE]);
void sequence(char *str,int *num,int *position);
int printfile(char *fname,int *strNum,int strNuml);


int main(int argc, char *argv[])
{
    int i=1,rez=0;
    int prntStrNum[MAX_SIZE]={0};
    char *inpNumStr=0,*inpFileName=0;


    while ( (rez = getopt(argc,argv,"f:l:")) != -1){
        switch (rez){
            case 'f':
                inpFileName=optarg;
                break;
            case 'l':
                inpNumStr=optarg;
                break;
            case 'h':
            case '?':
                printf("Use:\n-f\tinput filename\n-l\tnumber of lines\n");
                break;
        }
    }

    if(inpFileName==0 || inpNumStr==0)
    {
        fputs("Not specified input file or line numbers\n",stderr);
        return -1;
    }

    separation(inpNumStr,prntStrNum);
    if(printfile(inpFileName,prntStrNum,MAX_SIZE)!=0)
        fputs("Can not open input file",stderr);
    return 0;
}

void separation(char *str,int *strNum)
{
// Function input comma-separating string(1), truncate and save trancated strings in array.
// Array(2) reseived to function sequence
    char strBlocks[MAX_SIZE/4][MAX_SIZE/4]={0};
    char *p;
    int i=0,j=0,pos=0;
    int inNum=0;

    while(*str)                         //comma-separation string
    {
        if(*str!=',')
        {
            strBlocks[i][j++]=*str;
            inNum=1;
        }
        else if(*str==',')
        {
            strBlocks[i++][j]='\0';
            inNum=0;
            j=0;
        }

        if(i>MAX_SIZE/4 || j>MAX_SIZE/4)    //check out-of-bounds of the array
            break;

        str++;
    }
    strBlocks[i][j]='\0';

    p=strBlocks[0];
    while(*p)
    {
        sequence(p,strNum,&pos);
        p+=MAX_SIZE/4;
    }
}

void sequence(char *str,int *num,int *position)
{
/* Function input dash-separating string(1), calculate all numbers
    and save it in array(2) on start position(3)                  */

    int i=1,secNum=0,inNum=0,koeff=1;
    int first,second;

    if(strchr(str,'-')) //dash-separation string
    {
        while(*str)    // selecting first and second number
        {
            if(*str!='-' && secNum==0 && inNum==0)
            {
                first=atoi(str);
                inNum=1;
            }
            else if(*str!='-' && secNum==1 && inNum==0)
            {
                second=atoi(str);
                inNum=1;
            }
            else if(*str=='-')
            {
                secNum=1;
                inNum=0;
            }
            str++;
        }

        if(first<second)       //ascending
            koeff=1;
        else if(first>second)  //descending
            koeff=-1;

        while(first!=second)
        {
            if(*position<MAX_SIZE)
                *(num+(*position)++)=first;
            first+=koeff;
        }

        if(*position<MAX_SIZE)
            *(num+(*position)++)=first;
    }
    else //number
    {
        if(*position<MAX_SIZE)
            *(num+(*position)++)=atoi(str);
    }
}

int printfile(char *fname,int *strNum,int strNuml)
{
    FILE *f;
    char str[MAX_SIZE][MAX_SIZE*2]={'\0'};
    int i=1;
    int*p;

    if((f=fopen(fname,"r"))==0)
        return -1;

    while(!feof(f))  //read file before EOF
        fgets(str[i++],MAX_SIZE*2,f);

    p=strNum;
    while(p<strNum+strNuml)
    {
        if(*p<MAX_SIZE && *p && str[*p][0]!='\0')
            printf("%d: %s",*p,str[*p]);
        *p++;
    }

    fclose(f);
    return 0;
}
