#include<stdio.h>
#include<string.h>

#define SIZE 256
int main()
{
        FILE *file_in,*file_out;
        char arrStr[SIZE][SIZE];
        char *pStr[SIZE],*tmp,*flName[]={"lab4_job5_input.txt","lab4_job5_output.txt"};
        int endStr=0,i=0,x=0;
        
        if ((file_in = fopen("lab4_job4_input.txt", "r"))==0) 
            return -1;
        if ((file_out = fopen("lab4_job4_output.txt", "w"))==0) 
            return -1;

        while(!feof(file_in))  //input strings to array
        {
            fgets(arrStr[endStr],SIZE,file_in);
            if(arrStr[endStr][strlen(arrStr[endStr])-1]=='\n')
                arrStr[endStr][strlen(arrStr[endStr])-1]='\0';
            pStr[endStr]=arrStr[endStr];
            endStr++;
        }

        endStr--;
        
        while(i<=endStr) //sorting array of pointers
        {
            x=endStr;
            while(i<x) 
            {
                if(strlen(pStr[i])>strlen(pStr[x]))
                {
                    tmp=pStr[x];
                    pStr[x]=pStr[i];
                    pStr[i]=tmp;
                }
                x--;
            }
            fputs(pStr[i++],file_out);  //write to new file
            fputc('\n',file_out);
        }   
        
        fclose(file_in);
        fclose(file_out);
	    return 0;
}


// fwrite(pStr[i],strlen(pStr[i]),1,file_out);