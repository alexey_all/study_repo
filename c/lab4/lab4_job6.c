#include<stdio.h>
#include<string.h>

#define SIZE 256

int main()
{
    int age=0,relative=0,i=0,maxAge=-1,minAge=150;
	char names[SIZE][SIZE];
    char *young,*old;
	
	puts("Enter the number of relatives");
	if(scanf("%d", &relative)<1)
	{
	    puts("Incorrect data");
	    return -1;
	}
    fflush(stdin);

    while(i<relative)  //input names and calculate youngs and olds
    {
        printf("Input %d name\n", i+1);
        fgets(names[i],SIZE,stdin);
        names[i][strlen(names[i])-1]='\0';
        printf("Input age for %d name\n", i+1);
        if(scanf("%d", &age)<1)
        {
	    puts("Incorrect data");
	    return -1;
	}
        fflush(stdin);

        if(minAge>=age)
        {
            young=names[i];
            minAge=age;
        }
        if(maxAge<=age)
        {
            old=names[i];
            maxAge=age;
        }
        i++;
    }

    printf("%s is the youngest, she/he is %d years old\n",young,minAge);
    printf("%s is the oldest, she/he is %d years old\n",old,maxAge);
    return 0;
}