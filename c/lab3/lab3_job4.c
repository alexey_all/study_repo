#include<stdio.h>
#include<string.h>

int main()
{
        char str[256];
        char *p=str,*end;
        int inWord=0,len=0,summ=0,pow;
        
        printf("Enter a string of numbers\n");
        fgets(str,256,stdin);
        str[strlen(str)-1]='\0';
      
        while(1)
        {
            if(*p!=' ' && inWord==0)
            {  
                if(*p==0)
                    break;
                inWord=1;
                len++;
            }
            else if((*p==' ' || *p==0) && inWord==1) //если слово закончилось
            {
                end=p;
                pow=1;
                while(end-->p-len) //перебираем посимвольно до начала слова
                {
                    summ+=(*end-'0')*pow; //суммируем числа
                    if((end-(p-len))%10!=0)     //если больше 10 цифр в слове, разбиваем на подслова
                        pow*=10;
                    else
                        pow=1;
                }
                if(*p==0)
                    break;
                inWord=0;
                len=0;
            }
            else if(*p!=' ' && inWord==1)
            {      
                len++;
            }
            
            p++;
        }

        printf("Summ is %d\n", summ);
        
	return 0;
}