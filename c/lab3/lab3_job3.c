#include<stdio.h>
#include<string.h>

int main()
{
        char str[256];
        char *p=str,*start;
        int maxlen=0,inWord=0,len=0;
        
        printf("Enter a phrase\n");
        fgets(str,256,stdin);
        str[strlen(str)-1]='\0';
      
        while(*p)
        {
            if(*p!=' ' && inWord==0)
            {  
                inWord=1;
                len++;
            }
            else if(*p!=' ' && inWord==1)
            {
                len++;
            }
            else if(*p==' ' && inWord==1)
            {
                inWord=0;
                if(maxlen<len)
                {
                    maxlen=len;
                    start=p-len;
                }
                len=0;
            }
            
            p++;
        }
        
        while(*start!=' ')
            putchar(*start++);
            
    	printf(" %d\n",maxlen);
	
	return 0;
}