﻿#include<stdio.h>
#include<string.h>

int main()
{
	char str[256];
	char *p=str,*del=str,*end=str;
	int count=0,inWord=0,toDel;
        
	printf("Enter a phrase\n");
	fgets(str,256,stdin);
	str[strlen(str)-1]=0;
	printf("Enter a number of deleted word\n");
	if(scanf("%d",&toDel)!=1)
	{
	    printf("You have entered a wrong data\n");
	    return -1;	
	}

	while(*p)				 // подсчет количества слов
	{
	    if(*p!=' ' && inWord==0)
            {    
		count++;
		inWord=1;
            }  
            else if(*p==' ' && inWord==1)
		inWord=0;

	    if(count==toDel)     //удаляем лишнее
	    {
		del=p;
		end=p;
		
		while(*end!='\0' && *end!=' ')  //ищем конец слова 
		    end++;
		
		for(;end<=str+strlen(str);del++,end++) //сдвигаем
		    *del=*(end+1);
		
	        count++;
	    }
            p++;
	}


	if(toDel>count || toDel<0) 
	{
	    printf("%s\n","No such word");
	    return -1;
	}

	
	printf("%s\n",str);
	
	return 0;
}