#include<stdio.h>
#include<string.h>

int main()
{
        char str[256];
        char *p=str;
        int count=0,inWord=0;
        
        printf("Enter a phrase\n");
        fgets(str,256,stdin);
        str[strlen(str)-1]=0;
      
        while(*p)
        {
            if(*p!=' ' && inWord==0)
            {    
                count++;
                inWord=1;
            }    
            else if(*p==' ' && inWord==1)
                inWord=0;
            
            p++;
        }

    	printf("%d\n",count);
	
	return 0;
}