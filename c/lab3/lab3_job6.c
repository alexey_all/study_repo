#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<time.h>

#define SIZE 10

int main()
{
    int inSum=0,sum=0,mass[SIZE];
    int *p=mass,*max=mass,*min=mass;
		
    srand(time(0));

    while(p<mass+SIZE) 
    {
	*p=rand()%100-50;
	printf("%d ",*p);
	if(*p>*max) 
            max=p;
	else if(*p<*min) 
            min=p;
	p++;
    }

    putchar('\n');
    printf("Minimum is %d\n",*min);
    printf("Maximum is %d\n",*max);

    if(max>min)
    {
        while(max>min+1)
            sum+=*(++min);
    }
    else
    {
        while(min>max+1)
            sum+=*(++max);
    }

    printf("Total is %d\n",sum);
    return 0;
}