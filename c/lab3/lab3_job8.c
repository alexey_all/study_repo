﻿#include<stdio.h>
#include<string.h>

int main()
{
	char str[256];
	char *p=str;
	int count=0,inWord=0,toPrint;
        
	printf("Enter a phrase\n");
	fgets(str,256,stdin);
	str[strlen(str)-1]=0;
	printf("Enter a number of printed word\n");
	if(scanf("%d",&toPrint)!=1)
	{
	    printf("You have entered a wrong data\n");
	    return -1;	
	}

	while(*p)				 // подсчет количества слов
	{
	    if(*p!=' ' && inWord==0)
		{    
			count++;
			inWord=1;
        }
		else if(*p==' ' && inWord==1)
			inWord=0;

		if((*p)!=' ' && inWord==1 && count==toPrint)     //Выводим нужное
			putchar(*p);

        p++;
	}


	if(toPrint>count || toPrint<0) 
	{
	    printf("%s\n","No such word");
	    return -1;
	}

	
	putchar('\n');
	return 0;
}