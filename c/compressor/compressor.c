#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define SIZE 256


struct SYM
{
    unsigned char ch; //ASCII-���
    float freq; // ������� �������������
    char code[256]; // ������ ��� ������ ����
    struct SYM* left;
    struct SYM* right;
};

union CODE {
    unsigned char ch;
    struct {
        unsigned short b1:1;
        unsigned short b2:1;
        unsigned short b3:1;
        unsigned short b4:1;
        unsigned short b5:1;
        unsigned short b6:1;
        unsigned short b7:1;
        unsigned short b8:1;
     } byte;
};

int createTable(struct SYM* psym[], FILE *fl)
{
    int ch,i=0,j=0;
    int count[SIZE]={0};

    while((ch=fgetc(fl))!=EOF) // ������� �������
        count[ch]++;

    while(1)
    {
        psym[j]->freq=0;
        i=0;
        while(i<SIZE)
        {
            if(count[i]>0 && count[i]>psym[j]->freq)
            {
                psym[j]->freq=count[i];
                psym[j]->ch=i;
            }
            i++;
        }
        count[psym[j]->ch]=0;

        if(psym[j]->freq==0 || j>=SIZE-1) 
            break;
        j++;
    }
    return j;
}

struct SYM* buildTree(struct SYM* psym[], int N)
{
    int i=N;
    struct SYM *temp=(struct SYM*)malloc(sizeof(struct SYM));

    temp->freq=psym[N-2]->freq+psym[N-1]->freq;

    temp->left=psym[N-2];  // ���� �������� ������� N-2 � N-1, �� ������ ������������ 
    temp->right=psym[N-1]; // � ������� ��� ������������ � ����� ������ � ����� Lec �8
    temp->code[0]=0;
    if(N==2)
        return temp;

    while(temp->freq>psym[i]->freq && i>0)
    {
       psym[i+1]=psym[i]; 
       i--;
    }
    psym[++i]=temp;
   
    return buildTree(psym,N-1);
}

void makeCodes(struct SYM *root)
{
    if(root->left)
    {
        strcpy(root->left->code,root->code);
        strcat(root->left->code,"0");
        makeCodes(root->left);
    }
    if(root->right)
    {
        strcpy(root->right->code,root->code);
        strcat(root->right->code,"1");
        makeCodes(root->right);
    }
}

char* getCodes(struct SYM* letters,int cnt,int cntUniq,FILE *fp)
{
    int ch,i;
    char *codedStr,*tmpStr;
    codedStr=(char*)malloc(cnt*sizeof(char)*10);
    if(!codedStr)
        exit(1);

    tmpStr=codedStr;
    codedStr[0]='\0'; 
    fseek(fp,0,0); // ��������� � ������ �����

    while((ch=fgetc(fp))!=EOF)
    {
        for(i=0;i<=cntUniq;i++)
        {
            if(letters[i].ch==(unsigned char)ch) 
            {
                strcpy(tmpStr,letters[i].code);
                tmpStr+=strlen(letters[i].code);
                break; 
            }
        }
        //printf("%d - %c\n", x++, ch);
    }
    return codedStr;
}

unsigned char pack(unsigned char buf[])
{
    union CODE code;
    code.byte.b1=buf[0]-'0';
    code.byte.b2=buf[1]-'0';
    code.byte.b3=buf[2]-'0';
    code.byte.b4=buf[3]-'0';
    code.byte.b5=buf[4]-'0';
    code.byte.b6=buf[5]-'0';
    code.byte.b7=buf[6]-'0';
    code.byte.b8=buf[7]-'0';

    return code.ch;
}

void saveHead(struct SYM* letters,char* codedStr,char cntUniq,FILE* fw)
{
    //char* table[258]={0};
    int headSize=0;
    char tail=(8-(strlen(codedStr)%8))%8;
    char i=0;

    headSize=3+1+tail*sizeof(char)+cntUniq*sizeof(char)+cntUniq*sizeof(int);
    fwrite("!P!",3*sizeof(char),1,fw); // �������
    fwrite(&headSize,sizeof(int),1,fw);    // ������ �������

    while(i<cntUniq)  // ���������� ������� 
    {
        fwrite(&(letters[i].ch),sizeof(char),1,fw);
        fwrite(&(letters[i++].freq),sizeof(int),1,fw);
    }

    fwrite(&tail,sizeof(char),1,fw); //�����
}

void saveCode(char* codedStr,FILE* fw)
{
    char* packedStr;
    char tmp[2]={0};
    unsigned char buf[8]={0};
    int i=0,j=0,tail;

    tail=(8-(strlen(codedStr)%8))%8; // ����� ������
    
    while(*codedStr)   // ������ �����
    {
        buf[i++]=*codedStr;
        if(i>7)
        {
            i=0;
            tmp[0]=(char)pack(buf);
            fputs(tmp,fw);
        }
        codedStr++;
    }

    while(tail-j>0)    // ���������� �����, ���� �����
    {
        buf[i++]='0';
        j++;
        if(i>7)
        {
            tmp[0]=(char)pack(buf);
            fputs(tmp,fw);
        }
    }
}

int main(int argc, char *argv[])
{
    int i=0;
    int cntUniq=0; // ������� ���������� ��������
    char* codedStr; // ������������ ������� ������
    char inpFile[30],outFile[30];
    FILE *fr,*fw;
    struct SYM letters[SIZE];
    struct SYM* pletters[SIZE*2];
    struct SYM* root;

    strcpy(inpFile,"test.jpg");
    strcpy(outFile,inpFile);
    strcat(outFile,".pck");

    fr=fopen(inpFile,"rb");
    fw=fopen(outFile,"wb");
    
    if(fr==NULL || fw==NULL)
    {
        perror("File error: ");
        return -1;
    }

    while(i<SIZE)  // �������������� ������ ����������
    {
        pletters[i]=&letters[i];
        letters[i].left=letters[i].right=NULL;
        strcpy(letters[i++].code,"");
    }

    //First step
    cntUniq=createTable(pletters,fr);

    //Second step
    if(cntUniq<2)
        return -1;
    root=buildTree(pletters,cntUniq);
    makeCodes(root);

    //Third step
    codedStr=getCodes(letters,root->freq,cntUniq,fr);

    //Fourth step
    saveHead(letters,codedStr,(char)cntUniq,fw);
    saveCode(codedStr,fw);


    free(codedStr);
    fclose(fr);
    fclose(fw);
    return 0;
}  